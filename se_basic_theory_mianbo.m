function varargout = se_basic_theory_mianbo(varargin)
% SE_BASIC_THEORY_MIANBO MATLAB code for se_basic_theory_mianbo.fig
%      SE_BASIC_THEORY_MIANBO, by itself, creates a new SE_BASIC_THEORY_MIANBO or raises the existing
%      singleton*.
%
%      H = SE_BASIC_THEORY_MIANBO returns the handle to a new SE_BASIC_THEORY_MIANBO or the handle to
%      the existing singleton*.
%
%      SE_BASIC_THEORY_MIANBO('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in SE_BASIC_THEORY_MIANBO.M with the given input arguments.
%
%      SE_BASIC_THEORY_MIANBO('Property','Value',...) creates a new SE_BASIC_THEORY_MIANBO or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before se_basic_theory_mianbo_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to se_basic_theory_mianbo_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help se_basic_theory_mianbo

% Last Modified by GUIDE v2.5 21-Dec-2019 19:51:39

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @se_basic_theory_mianbo_OpeningFcn, ...
                   'gui_OutputFcn',  @se_basic_theory_mianbo_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before se_basic_theory_mianbo is made visible.
function se_basic_theory_mianbo_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to se_basic_theory_mianbo (see VARARGIN)

% Choose default command line output for se_basic_theory_mianbo
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes se_basic_theory_mianbo wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = se_basic_theory_mianbo_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


function pushbutton1_Callback(hObject, eventdata, handles)
set(gcf,'visible','off')
set(se_basic_theory,'visible','on')


function axes1_CreateFcn(hObject, eventdata, handles)
set(gca,'XColor',get(gca,'Color')) ;% 这两行代码功能：将坐标轴和坐标刻度转为白色
set(gca,'YColor',get(gca,'Color'));
set(gca,'XTickLabel',[]); % 这两行代码功能：去除坐标刻度
set(gca,'YTickLabel',[]);
a=imread('seismic_figure\面波.jpg');
imshow(a)
