function varargout = g_Transformation(varargin)
% g_Transformation MATLAB code for g_Transformation.fig
%      g_Transformation, by itself, creates a new g_Transformation or raises the existing
%      singleton*.
%
%      H = g_Transformation returns the handle to a new g_Transformation or the handle to
%      the existing singleton*.
%
%      g_Transformation('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in g_Transformation.M with the given input arguments.
%
%      g_Transformation('Property','Value',...) creates a new g_Transformation or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before g_Transformation_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to g_Transformation_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help g_Transformation

% Last Modified by GUIDE v2.5 08-Jun-2020 14:55:24

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @g_Transformation_OpeningFcn, ...
                   'gui_OutputFcn',  @g_Transformation_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before g_Transformation is made visible.
function g_Transformation_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to g_Transformation (see VARARGIN)

% Choose default command line output for g_Transformation
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes g_Transformation wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = g_Transformation_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(gcf,'visible','off');
gravity('visible','on');


function pushbutton3_Callback(hObject, eventdata, handles)
%% 读取重力数据
[filename,pathname]=uigetfile({'*.dat';'*.txt'},'请选择重力测量数据文件');
if ~filename==0
    str=[pathname filename];
    data=dlmread(str);%读取文件数据 
    axes(handles.axes1)
    x=data(:,1);
    y=data(:,2);
    dx = x(2) - x(1);
    %% 保存数据到handles结构体
    handles.gravityDataX=x;
    handles.gravityDataY=y;
    guidata(hObject,handles)%更新数据
    n=size(x,1);
    axes(handles.axes1)
    cla reset
    plot(x,y,'-ob')
    grid on
    xlabel('\bf测点号')
    ylabel('\bf重力异常值/g.u.')
    legend('原始数据','location','northwest')
else 
    msgbox('打开文件失败','massage');
end

function method_Callback(hObject, eventdata, handles)
choose = get(handles.method,'value');
    x = handles.gravityDataX;
    y = handles.gravityDataY;
    n = size(x,1);
    ySmooth = zeros(n,1);
switch choose
    case 1  %不做处理    
    case 2  %moving 移动平均法
        ySmooth = smooth(y,'moving');
        %选择几点平滑
        %如果点数在0~:25：三点；25~100：五点；101~200：七点；>200：九点
%         ySmooth(1) = y(1);
%         ySmooth(n) = y(n);
%         if(n<=25)
%             for i=2:n-1
%                 ySmooth(i) = sum(y(i-1:i+1))/3;
%             end
%         else
%             if(n<=100)
%                for i=2:n-1
%                     ySmooth(i) = sum(y(i-2:i+2))/5;
%                end 
%             else
%                 if(n<=200)
%                     for i=2:n-1
%                         ySmooth(i) = sum(y(i-3:i+3))/7;
%                     end
%                 else 
%                     for i=2:n-1
%                         ySmooth(i) = sum(y(i-4:i+4))/(9*20);
%                     end
%                 end
%             end
%         end
%         
    case 3  %lowess 局部回归
        %使用自带最小二乘法滤波函数sgolayfilt
        ySmooth = smooth(y,'lowess');
        
    case 4  %rlowess 局部回归
        ySmooth = smooth(y,'rlowess');
    case 5  %loess 局部回归
        ySmooth = smooth(y,'loess');
    case 6  %rloess 局部回归
        ySmooth = smooth(y,'rloess');
    case 7  %Sacitzky-Golay滤波
        ySmooth = smooth(y,'sgolay');
    case 8  %Sacitzky-Golay滤波
        ySmooth = y;
end
cla reset
dx = x(2) - x(1);
axes(handles.axes1)
plot(x,y,'-ob',x,ySmooth,'-*r')       
legend('原始数据','平滑滤波后','location','northwest')
grid on
xlabel('\bf测点号')
ylabel('\bf重力异常值/g.u.')
% set(gca,'xtick',0:dx:x(n))
handles.ySmooth = ySmooth;
guidata(hObject,handles);

axes(handles.axes2)
plot(x,ySmooth,'-ob')       
legend('预处理数据','location','northwest')
grid on
xlabel('\bf测点号')
ylabel('\bf重力异常值/g.u.')
% set(gca,'xtick',0:dx:x(n))

function method_CreateFcn(hObject, eventdata, handles)

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function axes1_CreateFcn(hObject, eventdata, handles)
grid on


function Transform_Callback(hObject, eventdata, handles)
temp = get(handles.Transform,'value');
x = handles.gravityDataX;
y = handles.ySmooth;
dx = x(2)-x(1);
% dy = y(2)-y(1);
n = size(x,1);
deltaY = zeros(n-2,1);
switch temp
    case 1
    case 2%平均场法
        for i = 2:n-1
            deltaY(i-1) = y(i) - (y(i-1)+y(i+1))/2;       
        end
        axes(handles.axes2)
        cla reset
        plot(x(2:n-1),deltaY,'-*r')
        grid on
        xlabel('\bf测点号')
        ylabel('\bf重力异常值/g.u.')
        % set(gca,'xtick',0:dx:x(n))
        %         set(gca,'ytick',min(deltaY):dy:max(deltaY))
        legend('分离转换后','location','northwest')
    case 3%高次导数法
%         for i = 2:n-1
%             deltaY(i-1) = (y(i-1)-2*y(i)+y(i+1))/(dx^2);
%         end
        y_1 = diff(y,1);
        y_2 = diff(y,2);
        axes(handles.axes2)
        cla reset
        plot(x(1:n-1),y_1,'-or',x(2:n-1),y_2,'-*g')
        grid on
        xlabel('\bf测点号')
        ylabel('\bf重力异常导数')
        % set(gca,'xtick',0:dx:x(n))
        %         set(gca,'ytick',min(deltaY):dy:max(deltaY))
        legend('\bf一阶导数','\bf二阶导数','location','northwest')
    case 4%解析延拓法
        nx = size(x,1);
        Za_up1=zeros(1,nx);
        h=1;   %延拓高度必须为点距dx的倍数，这里代表延拓高度为5m
        n=10;
        for i=(h*n+1):(nx-h*n)
            tmp=0;
            for j=(i-h*n):h:(i+h*n)
                k=(j-i)/h;
                tmp=tmp+y(j)*atan(8/(4*k*k+15))/pi;
            end
            Za_up1(i)=tmp;
        end
        cla reset
        plot(x,y,'-*r',x(h*n+1:nx-h*n),Za_up1(h*n+1:nx-h*n),'-ob')
        grid on
        xlabel('\bf测点号')
        ylabel('\bf重力异常向上延拓')
        l=num2str(abs(x(2)-x(1)));
        legend('\bf处理前',['\bf向上延拓' l 'm'],'location','best')
        
end


function Transform_CreateFcn(hObject, eventdata, handles)

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function pushbutton5_Callback(hObject, eventdata, handles)
%% 保存图片
 new_f_handle=figure('visible','off'); %新建一个不可见的figure
    new_axes=copyobj(handles.axes2,new_f_handle); %axes2是GUI界面内要保存图线的Tag，将其copy到不可见的figure中
    set(new_axes,'Units','normalized','Position',[0.1 0.1 0.8 0.8]);%将图线缩放
    [filename, pathname ,fileindex]=uiputfile({'*.png';'*.bmp';'*.jpg';'*.eps';},'图片保存为');
    if  filename~=0%未点“取消”按钮或未关闭
        file=strcat(pathname,filename);
        switch fileindex %根据不同的选择保存为不同的类型        
            case 1
                print(new_f_handle,'-dpng',file);% print(new_f_handle,'-dpng',filename);效果一样，将图像打印到指定文件中
                msgbox({'图片保存成功：',pathname,filename},'提示')
%                 fprintf('>>已保存到：%s\n',file);
            case 2
                print(new_f_handle,'-dbmp',file);
                msgbox({'图片保存成功：',pathname,filename},'提示')
%                 fprintf('>>已保存到：%s\n',file);
            case 3
                print(new_f_handle,'-djpeg',file);
                msgbox({'图片保存成功：',pathname,filename},'提示')
%                 fprintf('>>已保存到：%s\n',file);
            case 4
                print(new_f_handle,'-depsc',file);
                msgbox({'图片保存成功：',pathname,filename},'提示')
%                 fprintf('>>已保存到：%s\n',file);
        end 
    else 
        msgbox('保存失败！','massage');    
    end


function axes2_CreateFcn(hObject, eventdata, handles)
