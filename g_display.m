function varargout = g_display(varargin)
% G_DISPLAY MATLAB code for g_display.fig
%      G_DISPLAY, by itself, creates a new G_DISPLAY or raises the existing
%      singleton*.
%
%      H = G_DISPLAY returns the handle to a new G_DISPLAY or the handle to
%      the existing singleton*.
%
%      G_DISPLAY('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in G_DISPLAY.M with the given input arguments.
%
%      G_DISPLAY('Property','Value',...) creates a new G_DISPLAY or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before g_display_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to g_display_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help g_display

% Last Modified by GUIDE v2.5 14-Nov-2019 00:21:09

% Begin initialization code - DO NOT EDIT
gui_Singleton = 0;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @g_display_OpeningFcn, ...
                   'gui_OutputFcn',  @g_display_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before g_display is made visible.
function g_display_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to g_display (see VARARGIN)

% Choose default command line output for g_display
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes g_display wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = g_display_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(gcf,'visible','off');
gravity('visible','on');


function figure1_SizeChangedFcn(hObject, eventdata, handles)


function popupmenu1_Callback(hObject, eventdata, handles)
sle=get(hObject,'value');
axis off;
switch sle
    case 1
    case 2  %地球真实形状及重力场
        set(handles.sel2,'visible','off')
        set(handles.sel3,'visible','off')
        axes(handles.axes1);
        a=imread('the_earth.jpg');
        imshow(a);
        set(handles.explain,'string',...
            '      地球的真实形状为一个两极稍扁、赤道略鼓的不规则球体,地球的重力场值分布也和地球本身的形状密不可分，实际上重力异常可以看做是一种体积效应。')
    case 3  %重力改正
        set(handles.sel2,'visible','off')
        set(handles.sel3,'visible','off')
        axes(handles.axes1);
        cla reset
        set(gca,'XColor',get(gca,'Color')) ;% 这两行代码功能：将坐标轴和坐标刻度转为白色
        set(gca,'YColor',get(gca,'Color'));
        set(gca,'XTickLabel',[]); % 这两行代码功能：去除坐标刻度
        set(gca,'YTickLabel',[]);
        set(handles.explain,'string',...
            '重力改正包括地形改正、中间层改正、自由空气改正、正常场改正以及均衡改正')
        set(handles.sel2,'visible','on')
    case 4  %重力异常
        set(handles.sel2,'visible','off')
        set(handles.sel3,'visible','on')
        
   
end
        


function popupmenu1_CreateFcn(hObject, eventdata, handles)

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function explain_Callback(hObject, eventdata, handles)


function explain_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function axes1_CreateFcn(hObject, eventdata, handles)
set(gca,'XColor',get(gca,'Color')) ;% 这两行代码功能：将坐标轴和坐标刻度转为白色
set(gca,'YColor',get(gca,'Color'));
 
set(gca,'XTickLabel',[]); % 这两行代码功能：去除坐标刻度
set(gca,'YTickLabel',[]);


function sel2_Callback(hObject, eventdata, handles)
sel=get(hObject,'value');
axes(handles.axes1)
switch sel
    case 1
    case 2 %改正之前
        a1=imread('Gravity_correction1.png');
        imshow(a1);
    case 3 %地形改正
        a2=imread('Gravity_correction2.png');
        set(handles.explain,'string','地形改正在于消除测点之间地形起伏带来的影响，使得测点在同一水平面')
        imshow(a2);
    case 4 %自由空气改正
        a3=imread('Gravity_correction3.png');
        imshow(a3);
        set(handles.explain,'string','自由空气改正又称高度校正，假设参考椭球面以及测点之间没有岩石存在，而只有自由的空气，消除测点高程差异')        
    case 5 %正常场改正
        a4=imread('Gravity_correction4.png');
        imshow(a4);
        set(handles.explain,'string',...
            '正常场改正在于从观测值中消除密度为正常分布(即地壳的平均密度2.67g/cm^3)的大地椭球面的正常重力值')        
    case 6 %均衡改正
        a5=imread('Gravity_correction5.png');
        imshow(a5);
        set(handles.explain,'string','均衡改正的目地是依据某种均衡模式调整地壳，通过将地形质量移到大地水准面的内部，以弥补山下的质量亏损')  
    case 7 %中间层改正
        a6=imread('Gravity_correction6.png');
        imshow(a6);
        set(handles.explain,'string','中间层改正的目地消除地形校正后测点水平面与大地水准面这一中间层的影响')
end
function sel2_CreateFcn(hObject, eventdata, handles)

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function sel3_Callback(hObject, eventdata, handles)
sel=get(hObject,'value');
axes(handles.axes1)
switch sel
    case 1
    case 2 %第一种自由空间重力异常
        a1=imread('gravity_anomaly1.png');
        set(handles.explain,'string','第一种自由空间重力异常=观测值+自由空气校正+正常场改正');
        imshow(a1)
    case 3 %第二种自由空间重力异常
        a2=imread('gravity_anomaly2.png');
        set(handles.explain,'string','第二种自由空间重力异常=观测值+自由空气校正+正常场改正+地形改正');
        imshow(a2)
    case 4 %布格重力异常
        a3=imread('gravity_anomaly3.png');
        set(handles.explain,'string','布格重力异常=观测值+布格改正(自由空气校正+中间层改正)+正常场改正+地形改正');
        imshow(a3)
    case 5 %均衡重力异常
        a4=imread('gravity_anomaly4.png');
        set(handles.explain,'string','布格重力异常=观测值+布格改正(自由空气校正+中间层改正)+正常场改正+地形改正+均衡改正');
        imshow(a4)
end

function sel3_CreateFcn(hObject, eventdata, handles)

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
