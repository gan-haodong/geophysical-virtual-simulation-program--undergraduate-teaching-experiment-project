function [U]=DC1D_Forward(rho1,h1,r)
global rho;
global h;
rho=rho1;
h  =h1;
U=zeros(size(r));
z=U;
for i=1:size(r,2)
    	 z(i) = hankel_801('Transfer_Fun',r(i));
         U(i)=z(i)/(2*pi);         
end
