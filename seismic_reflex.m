function varargout = seismic_reflex(varargin)
% SEISMIC_REFLEX MATLAB code for seismic_reflex.fig
%      SEISMIC_REFLEX, by itself, creates a new SEISMIC_REFLEX or raises the existing
%      singleton*.
%
%      H = SEISMIC_REFLEX returns the handle to a new SEISMIC_REFLEX or the handle to
%      the existing singleton*.
%
%      SEISMIC_REFLEX('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in SEISMIC_REFLEX.M with the given input arguments.
%
%      SEISMIC_REFLEX('Property','Value',...) creates a new SEISMIC_REFLEX or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before seismic_reflex_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to seismic_reflex_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help seismic_reflex

% Last Modified by GUIDE v2.5 11-Mar-2020 15:42:40

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @seismic_reflex_OpeningFcn, ...
                   'gui_OutputFcn',  @seismic_reflex_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before seismic_reflex is made visible.
function seismic_reflex_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to seismic_reflex (see VARARGIN)

% Choose default command line output for seismic_reflex
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes seismic_reflex wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = seismic_reflex_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


function pushbutton1_Callback(hObject, eventdata, handles)
set(gcf,'visible','off')
set(seismic,'visible','on')
global stop;
stop = true;

function axes1_CreateFcn(hObject, eventdata, handles)
%% 去除坐标轴

set(gca,'XColor',get(gca,'Color')) ;% 这两行代码功能：将坐标轴和坐标刻度转为白色
set(gca,'YColor',get(gca,'Color'));
set(gca,'XTickLabel',[]); % 这两行代码功能：去除坐标刻度
set(gca,'YTickLabel',[]);

function edit1_Callback(hObject, eventdata, handles)


function edit1_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function pushbutton2_Callback(hObject, eventdata, handles)
global stop;
stop = false;
axes(handles.axes1)
set(handles.edit1,'string','地震反射波法，利用地震反射波进行人工地震勘探的方法，是大陆架油气勘探的首要手段。通过获得地震剖面，解决构造问题、岩性问题。优点是精度高、分辨率高、勘探深度大，其缺点在于常规反射地震法观测方位受限（只能在地表观测），无法获得全息信息，因而，影响到解决高陡构造问题的能力。')
for i=1:3
    if(~stop)
        img = imread(['seismic_figure\seismicReflex',num2str(i),'.jpg']);
        imshow(img)
        pause(1.5)
    end
end
