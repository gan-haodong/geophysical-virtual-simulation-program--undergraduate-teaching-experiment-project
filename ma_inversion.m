function varargout = ma_inversion(varargin)
% MA_INVERSION MATLAB code for ma_inversion.fig
%      MA_INVERSION, by itself, creates a new MA_INVERSION or raises the existing
%      singleton*.
%
%      H = MA_INVERSION returns the handle to a new MA_INVERSION or the handle to
%      the existing singleton*.
%
%      MA_INVERSION('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in MA_INVERSION.M with the given input arguments.
%
%      MA_INVERSION('Property','Value',...) creates a new MA_INVERSION or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before ma_inversion_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to ma_inversion_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help ma_inversion

% Last Modified by GUIDE v2.5 29-Aug-2019 10:26:57

% Begin initialization code - DO NOT EDIT
gui_Singleton = 0;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @ma_inversion_OpeningFcn, ...
                   'gui_OutputFcn',  @ma_inversion_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before ma_inversion is made visible.
function ma_inversion_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to ma_inversion (see VARARGIN)

% Choose default command line output for ma_inversion
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes ma_inversion wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = ma_inversion_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% handles    structure with handles and user data (see GUIDATA)
set(gcf,'visible','off');
magnetic('visible','on');
