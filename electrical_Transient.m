function varargout = electrical_Transient(varargin)
% ELECTRICAL_TRANSIENT MATLAB code for electrical_Transient.fig
%      ELECTRICAL_TRANSIENT, by itself, creates a new ELECTRICAL_TRANSIENT or raises the existing
%      singleton*.
%
%      H = ELECTRICAL_TRANSIENT returns the handle to a new ELECTRICAL_TRANSIENT or the handle to
%      the existing singleton*.
%
%      ELECTRICAL_TRANSIENT('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in ELECTRICAL_TRANSIENT.M with the given input arguments.
%
%      ELECTRICAL_TRANSIENT('Property','Value',...) creates a new ELECTRICAL_TRANSIENT or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before electrical_Transient_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to electrical_Transient_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help electrical_Transient

% Last Modified by GUIDE v2.5 09-Mar-2020 12:06:35

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @electrical_Transient_OpeningFcn, ...
                   'gui_OutputFcn',  @electrical_Transient_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before electrical_Transient is made visible.
function electrical_Transient_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to electrical_Transient (see VARARGIN)

% Choose default command line output for electrical_Transient
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes electrical_Transient wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = electrical_Transient_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


function pushbutton1_Callback(hObject, eventdata, handles)
global stop;
stop = true;
axes(handles.axes1)
cla reset
set(gcf,'visible','off')
set(electrical,'visible','on')


function axes1_CreateFcn(hObject, eventdata, handles)
%% 去除坐标轴
set(gca,'XColor',get(gca,'Color')) ;% 这两行代码功能：将坐标轴和坐标刻度转为白色
set(gca,'YColor',get(gca,'Color'));
set(gca,'XTickLabel',[]); % 这两行代码功能：去除坐标刻度
set(gca,'YTickLabel',[]);


function pushbutton2_Callback(hObject, eventdata, handles)
global stop;
stop = false;
axes(handles.axes1)
for i=1:3
    if(~stop)
        img = imread(['electricTran\ATEM',num2str(i),'.jpg']);
        imshow(img)
        pause(1.5)
    end
end
