function varargout = g_application(varargin)
% G_APPLICATION MATLAB code for g_application.fig
%      G_APPLICATION, by itself, creates a new G_APPLICATION or raises the existing
%      singleton*.
%
%      H = G_APPLICATION returns the handle to a new G_APPLICATION or the handle to
%      the existing singleton*.
%
%      G_APPLICATION('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in G_APPLICATION.M with the given input arguments.
%
%      G_APPLICATION('Property','Value',...) creates a new G_APPLICATION or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before g_application_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to g_application_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help g_application

% Last Modified by GUIDE v2.5 09-Mar-2020 12:19:27

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @g_application_OpeningFcn, ...
                   'gui_OutputFcn',  @g_application_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before g_application is made visible.
function g_application_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to g_application (see VARARGIN)

% Choose default command line output for g_application
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes g_application wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = g_application_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(gcf,'visible','off');
gravity('visible','on');


function edit1_Callback(hObject, eventdata, handles)

function edit1_CreateFcn(hObject, eventdata, handles)
% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function figure1_CreateFcn(hObject, eventdata, handles)


function pushbutton4_Callback(hObject, eventdata, handles)
set(handles.edit1,'string','1、利用小比例尺(1:100万-1:50万)重力异常图研究区域地质构造，划分构造单元，圈定沉积盆地的范围，预测含油、气远景区；2、根据中等比例尺(1:20-1:10万)的重力异常图划分沉积盆地内的次一级构造，进一步圈定出有利于油、气藏形成的地段，寻找局部构造，如地层构造、古潜山、盐丘、地层尖灭、断层封闭等有利于油、气藏储存的地段；3、利用大比例尺高精度重力测量查明与油、气藏有关的局部构造的细节，直接寻找与油、气藏有关的低密度体，为钻井布置提供依据；4、在油气开发过程中，根据重力异常随时间的变化，可以监测油气藏的开发过程')
