function varargout = electrical_Direct_Principle(varargin)
% ELECTRICAL_DIRECT_PRINCIPLE MATLAB code for electrical_Direct_Principle.fig
%      ELECTRICAL_DIRECT_PRINCIPLE, by itself, creates a new ELECTRICAL_DIRECT_PRINCIPLE or raises the existing
%      singleton*.
%
%      H = ELECTRICAL_DIRECT_PRINCIPLE returns the handle to a new ELECTRICAL_DIRECT_PRINCIPLE or the handle to
%      the existing singleton*.
%
%      ELECTRICAL_DIRECT_PRINCIPLE('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in ELECTRICAL_DIRECT_PRINCIPLE.M with the given input arguments.
%
%      ELECTRICAL_DIRECT_PRINCIPLE('Property','Value',...) creates a new ELECTRICAL_DIRECT_PRINCIPLE or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before electrical_Direct_Principle_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to electrical_Direct_Principle_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help electrical_Direct_Principle

% Last Modified by GUIDE v2.5 05-Mar-2020 20:27:38

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @electrical_Direct_Principle_OpeningFcn, ...
                   'gui_OutputFcn',  @electrical_Direct_Principle_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before electrical_Direct_Principle is made visible.
function electrical_Direct_Principle_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to electrical_Direct_Principle (see VARARGIN)

% Choose default command line output for electrical_Direct_Principle
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes electrical_Direct_Principle wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = electrical_Direct_Principle_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


function pushbutton1_Callback(hObject, eventdata, handles)
cla reset
global stop;
stop = true;
set(gcf,'visible','off')
set(electrical_Direct,'visible','on')


function axes1_CreateFcn(hObject, eventdata, handles)
%% 
        %% 去除坐标轴
        
        set(gca,'XColor',get(gca,'Color')) ;% 这两行代码功能：将坐标轴和坐标刻度转为白色
        set(gca,'YColor',get(gca,'Color'));
        set(gca,'XTickLabel',[]); % 这两行代码功能：去除坐标刻度
        set(gca,'YTickLabel',[]);
        %% 显示图片
% %         axes(handles.axes1)
%         for i=1:5
%             ['G:\我的大学\物探虚拟仿真软件\electricDirect\',num2str(i),'.jpg']
%             img = imread(['G:\我的大学\物探虚拟仿真软件\electricDirect\',num2str(i),'.jpg']);
% %             img = imread('G:\我的大学\物探虚拟仿真软件\electricDirect\1.jpg');
%             imshow(img)
%             pause(1)
%         end
        
%% 


% function pushbutton2_Callback(hObject, eventdata, handles)
% for i=1:5
%     img = imread(['G:\我的大学\物探虚拟仿真软件\electricDirect\',num2str(i),'.jpg']);
% %             img = imread('G:\我的大学\物探虚拟仿真软件\electricDirect\1.jpg');
%     imshow(img)
%     pause(1)
% end


function pushbutton3_Callback(hObject, eventdata, handles)
global stop;
stop = false;
path = pwd;
for i=1:5
    if(~stop)
        img = imread([path,'\electricDirect\',num2str(i),'.jpg']);
    %             img = imread('G:\我的大学\物探虚拟仿真软件\electricDirect\1.jpg');
        imshow(img)
        pause(1.5)
    end
end