function varargout = electrical_frequency(varargin)
% ELECTRICAL_FREQUENCY MATLAB code for electrical_frequency.fig
%      ELECTRICAL_FREQUENCY, by itself, creates a new ELECTRICAL_FREQUENCY or raises the existing
%      singleton*.
%
%      H = ELECTRICAL_FREQUENCY returns the handle to a new ELECTRICAL_FREQUENCY or the handle to
%      the existing singleton*.
%
%      ELECTRICAL_FREQUENCY('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in ELECTRICAL_FREQUENCY.M with the given input arguments.
%
%      ELECTRICAL_FREQUENCY('Property','Value',...) creates a new ELECTRICAL_FREQUENCY or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before electrical_frequency_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to electrical_frequency_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help electrical_frequency

% Last Modified by GUIDE v2.5 08-Mar-2020 14:19:16

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @electrical_frequency_OpeningFcn, ...
                   'gui_OutputFcn',  @electrical_frequency_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before electrical_frequency is made visible.
function electrical_frequency_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to electrical_frequency (see VARARGIN)

% Choose default command line output for electrical_frequency
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes electrical_frequency wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = electrical_frequency_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


function pushbutton1_Callback(hObject, eventdata, handles)
global stop;
stop = [0,0,0];
set(gcf,'visible','off')
set(electrical,'visible','on')


function popupmenu1_Callback(hObject, eventdata, handles)
global stop;
stop = [0,0,0];
choose = get(handles.popupmenu1,'value');
axes(handles.huitumingzi)
switch choose 
    case 1
        
    case 2%MT
        stop = [1,0,0];
        set(handles.edit2,'string','在某个观测点上，测量由天然电磁场源产生的电场和磁场的水平分量。二者的比值称为电性阻抗。在一系列频率点上确定阻抗，了解地球内部导电率随深度变化的信息。若地下介质是各向同性的，则电场强度和磁场强度互相垂直。一般地，天然交变电磁场的频率范围约为10^4 ～10^(-4) Hz，频率越低，穿透深度越大，在地面测点观测不同频率天然电磁场信号，并进行分析处理，就能得到地球不同深度处的电性分布。')
        for i=1:3
            if(stop(1))
                img = imread(['electricFre\MT',num2str(i),'.jpg']);
                imshow(img)
                pause(1.5)
            end
        end
        case 3%CSAMT
            stop = [0,1,0];
            set(handles.edit2,'string','可控源音频大地电磁法(CSAMT)，采用人工场源供电，其频率范围为0.25~8192Hz。由于CSAMT法所观测电磁场的频率范围、场强和方向可由人工控制，其观测方式又与MT法相同，所以称为“可控源音频大地电磁法”。')
            for i=1:3
                if(stop(2))
                    img = imread(['electricFre\CSAMT',num2str(i),'.jpg']);
                    imshow(img)
                    pause(1.5)
                end
            end
    case 4%WFEM
        stop = [0,0,1];
        set(handles.edit2,'string','广域电磁法(WFEM)是相对于传统的可控源音频大地电磁（CSAMT）法和MELOS方法提出来的。该方法继承了CSAMT法使用人工场源克服场源随机性的优点，也继承了MELOS方法非远区测量的优势；摒弃了CSAMT法远区信号微弱的劣势，扩展了观测适用范围，同时也摒弃了MELOS方法的校正办法，保留了计算公式中的高次项；既不沿用卡尼亚公式，也不把非远区校正到近区，而是用适合于全域的公式计算视电阻率，大大拓展了人工源电磁法的观测范围，提高了观测速度、精度和野外工作效率。广域电磁法和伪随机信号电法结合起来，形成了独具特色的一种新的电法勘探方法。')
        for i=1:3
            if(stop(3))
                img = imread(['electricFre\WFEM',num2str(i),'.jpg']);
                imshow(img)
                pause(1.5)
            end
        end
end

function popupmenu1_CreateFcn(hObject, eventdata, handles)

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function edit2_Callback(hObject, eventdata, handles)


function edit2_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function huitumingzi_CreateFcn(hObject, eventdata, handles)
%% 去除坐标轴

set(gca,'XColor',get(gca,'Color')) ;% 这两行代码功能：将坐标轴和坐标刻度转为白色
set(gca,'YColor',get(gca,'Color'));
set(gca,'XTickLabel',[]); % 这两行代码功能：去除坐标刻度
set(gca,'YTickLabel',[]);


function MT_forward_Callback(hObject, eventdata, handles)
global stop;
stop = [0,0,0];
set(gcf,'visible','off')
set(MT1D,'visible','on')

function pushbutton4_Callback(hObject, eventdata, handles)
global stop;
stop = [0,0,0];
set(gcf,'visible','off')
set(EM1D,'visible','on')
