function varargout = electrical_Direct_observe(varargin)
% ELECTRICAL_DIRECT_OBSERVE MATLAB code for electrical_Direct_observe.fig
%      ELECTRICAL_DIRECT_OBSERVE, by itself, creates a new ELECTRICAL_DIRECT_OBSERVE or raises the existing
%      singleton*.
%
%      H = ELECTRICAL_DIRECT_OBSERVE returns the handle to a new ELECTRICAL_DIRECT_OBSERVE or the handle to
%      the existing singleton*.
%
%      ELECTRICAL_DIRECT_OBSERVE('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in ELECTRICAL_DIRECT_OBSERVE.M with the given input arguments.
%
%      ELECTRICAL_DIRECT_OBSERVE('Property','Value',...) creates a new ELECTRICAL_DIRECT_OBSERVE or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before electrical_Direct_observe_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to electrical_Direct_observe_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help electrical_Direct_observe

% Last Modified by GUIDE v2.5 07-Mar-2020 15:27:03

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @electrical_Direct_observe_OpeningFcn, ...
                   'gui_OutputFcn',  @electrical_Direct_observe_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before electrical_Direct_observe is made visible.
function electrical_Direct_observe_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to electrical_Direct_observe (see VARARGIN)

% Choose default command line output for electrical_Direct_observe
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes electrical_Direct_observe wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = electrical_Direct_observe_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


function pushbutton1_Callback(hObject, eventdata, handles)
cmd='mtGifShowByQt\my_GifShow.exe';
system(cmd);

function pushbutton3_Callback(hObject, eventdata, handles)
cmd='taskkill /f /t /im my_GifShow.exe';
system(cmd)


function pushbutton4_Callback(hObject, eventdata, handles)
set(gcf,'visible','off');
set(electrical_Direct,'visible','on');


function popupmenu1_Callback(hObject, eventdata, handles)
choose = get(handles.popupmenu1,'value');
axes(handles.axes1)
switch choose
    case 1
    case 2%中间梯度装置
        img = imread('electricDirect\观测装置\中间梯度装置.jpg');
        imshow(img)
        set(handles.explain,'string',['中间梯度装置原理图如图所示，','A、B供电极放置在两侧间隔一定距离固定不动，','且关于测线中心对称，测量极M、N在AB中间三分之一地段逐点测量。','此外，中梯装置还可在离开 AB 连线一定距离且平行于 AB 的旁侧线上进行观测。'])
    case 3%三极装置
        img = imread('electricDirect\观测装置\三极装置.jpg');
        imshow(img)
        set(handles.explain,'string','三极装置原理图如图所示，其中供电电极 B 置于无穷远，记录点为 MN 中点 O。进行剖面实验时，AM、AN保持不变，A、MN在测线上同时移动测量。测深实验时，将MN放置于测量中心，通过逐渐增大AM的大小来增大测量深度。三极装置分为A极供电、B极无穷远和B极供电、A极无穷远两种情况，将两个三极装置联合起来便组成了联合剖面装置。')

    case 4%二极装置
        img = imread('electricDirect\观测装置\二极装置.jpg');
        imshow(img)
        set(handles.explain,'string','二极装置示意图如图所示，供电电极 B 和测量电极 N 均置于无穷远。实验中，将供电电极 B 和测量电极 N 接在实验室外面的接地线上，近似认为无穷远。')

    case 5%偶极装置
        img = imread('electricDirect\观测装置\偶极装置.jpg');
        imshow(img)
        set(handles.explain,'string','偶极装置示意图如图所示，供电电极 AB 和测量电极 MN 均采用偶极并分开有一定距离，四个电极都在一条测线上。AB=MN=d（d为偶极子长度），OO’=nd，n为电极的间隔系数。在进行剖面实验时，保持AB、MN的相对位置不变，在测线上从左到右一起移动测量；在进行测深实验时，将AB、MN关于测线中心对称放置，AB、MN反方向移动，并保持关于测线中心对称，通过增大OO’的大小来改变测量深度。')

    case 6%对称四极装置
        img = imread('electricDirect\观测装置\对称四极装置.jpg');
        imshow(img)
        set(handles.explain,'string','对称四极装置示意图如图所示，对称四极装置的特点是 AM=NB ，记录点取在 MN 中点O。 在进行剖面实验时，保持AB、MN的相对位置不变，在测线上从左到右整体移动测量；在进行测深实验时，将MN放置在测线中心，逐渐增大AB距离，来改变测量深度。')

    case 7%温纳装置
        img = imread('electricDirect\观测装置\温纳装置.jpg');
        imshow(img)
        set(handles.explain,'string','温纳装置示意图如图所示，温纳装置是一种特殊的对称四极装置，其特点为 AM = MN = NB = a 。其剖面与测深操作方法与对称四极装置相同。')

end


function popupmenu1_CreateFcn(hObject, eventdata, handles)

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function explain_Callback(hObject, eventdata, handles)


function explain_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function axes1_CreateFcn(hObject, eventdata, handles)
%%         %% 去除坐标轴
set(gca,'XColor',get(gca,'Color')) ;% 这两行代码功能：将坐标轴和坐标刻度转为白色
set(gca,'YColor',get(gca,'Color'));
set(gca,'XTickLabel',[]); % 这两行代码功能：去除坐标刻度
set(gca,'YTickLabel',[]);
%% 
