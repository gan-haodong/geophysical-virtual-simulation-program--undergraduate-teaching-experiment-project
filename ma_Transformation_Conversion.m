function varargout = ma_Transformation_Conversion(varargin)
% MA_TRANSFORMATION_CONVERSION MATLAB code for ma_Transformation_Conversion.fig
%      MA_TRANSFORMATION_CONVERSION, by itself, creates a new MA_TRANSFORMATION_CONVERSION or raises the existing
%      singleton*.
%
%      H = MA_TRANSFORMATION_CONVERSION returns the handle to a new MA_TRANSFORMATION_CONVERSION or the handle to
%      the existing singleton*.
%
%      MA_TRANSFORMATION_CONVERSION('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in MA_TRANSFORMATION_CONVERSION.M with the given input arguments.
%
%      MA_TRANSFORMATION_CONVERSION('Property','Value',...) creates a new MA_TRANSFORMATION_CONVERSION or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before ma_Transformation_Conversion_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to ma_Transformation_Conversion_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help ma_Transformation_Conversion

% Last Modified by GUIDE v2.5 22-Sep-2019 18:39:17

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @ma_Transformation_Conversion_OpeningFcn, ...
                   'gui_OutputFcn',  @ma_Transformation_Conversion_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before ma_Transformation_Conversion is made visible.
function ma_Transformation_Conversion_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to ma_Transformation_Conversion (see VARARGIN)

% Choose default command line output for ma_Transformation_Conversion
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes ma_Transformation_Conversion wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = ma_Transformation_Conversion_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


function pushbutton1_Callback(hObject, eventdata, handles)
set(gcf,'visible','off')
ma_Transformation('visible','on');


function pushbutton2_Callback(hObject, eventdata, handles)
%% 确认按钮
cla reset
Distance=str2double(get(handles.Distance,'string'));    %测点点距
x=(-40:1:40)*Distance;
A=pi*str2double(get(handles.A,'string'))/180;
I=pi*str2double(get(handles.I,'string'))/180;
if (A==pi/2)||(I==pi/2)
    	Is=pi/2;
else
    	Is=atan(tan(I)/cos(A));
end
R=str2double(get(handles.R,'string'));
B=50000;
k=str2double(get(handles.k,'string'));
r =str2double(get(handles.r,'string'));
mu_0 = 4*pi*1e-7;
Ms=k*B/mu_0;
S=pi*r*r;
ms=Ms*S;
for i=1:size(x,2) 
        	Za(i)=(mu_0/(2*pi))*ms*((R*R-x(i)*x(i))*sin(Is)...
              	-2*R*x(i)*cos(Is))/((R^2+x(i)^2)^2);
        	Ha(i)=-(mu_0/(2*pi))*ms*((R*R-x(i)*x(i))*cos(Is)...
              	+2*R*x(i)*sin(Is))/((R^2+x(i)^2)^2);
end
axes(handles.axes1)
plot(x, Za,'.-b');
hold on
plot(x, Ha,'+-g');
c=[0.4286 0.1749 0.1103 0.0813 0.0645 0.0536 0.0458...
    	0.0400 0.0355 0.1759];%转换系数
N=size(c,2); 
for i=(N+1):(size(Za,2)-N)
    	haxz=0;
    	for j=1:N
        	haxz=haxz+c(j)*(Za(i+j)-Za(i-j));
    	end
    	haxzh(i-N)=haxz;
    	x1(i-N)=x(i);
end
plot(x1, haxzh,'*m-');
xyplot
legend('Za','Ha理论曲线','Za→Ha计算曲线','location','best');
legend('boxoff')
set(gca,'fontsize',8)
%% 


function r_Callback(hObject, eventdata, handles)


function r_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function R_Callback(hObject, eventdata, handles)


function R_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function I_Callback(hObject, eventdata, handles)


function I_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function Distance_Callback(hObject, eventdata, handles)


function Distance_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function A_Callback(hObject, eventdata, handles)


function A_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function k_Callback(hObject, eventdata, handles)


function k_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function pushbutton3_Callback(hObject, eventdata, handles)
%% 保存图片
    new_f_handle=figure('visible','off'); %新建一个不可见的figure
    new_axes=copyobj(handles.axes1,new_f_handle); %axes1是GUI界面内要保存图线的Tag，将其copy到不可见的figure中
    set(new_axes,'Units','normalized','Position',[0.1 0.1 0.8 0.8]);%将图线缩放，units是计量单位，position =  [x y width height]
    [filename, pathname ,fileindex]=uiputfile({'*.png';'*.bmp';'*.jpg';'*.eps';},'图片保存为');
    if  filename~=0%未点“取消”按钮或未关闭
        file=strcat(pathname,filename);
        switch fileindex %根据不同的选择保存为不同的类型        
            case 1
                print(new_f_handle,'-dpng',file);% print(new_f_handle,'-dpng',filename);效果一样，将图像打印到指定文件中
                msgbox({'图片保存成功：',pathname,filename},'提示')
%                 fprintf('>>已保存到：%s\n',file);
            case 2
                print(new_f_handle,'-dbmp',file);
                msgbox({'图片保存成功：',pathname,filename},'提示')
%                 fprintf('>>已保存到：%s\n',file);
            case 3
                print(new_f_handle,'-djpeg',file);
                msgbox({'图片保存成功：',pathname,filename},'提示')
%                 fprintf('>>已保存到：%s\n',file);
            case 4
                print(new_f_handle,'-depsc',file);
                msgbox({'图片保存成功：',pathname,filename},'提示')
%                 fprintf('>>已保存到：%s\n',file);
        end 
    else 
        msgbox('保存失败！','massage');    
    end
