function varargout = ma_Transformation(varargin)
% MA_TRANSFORMATION MATLAB code for ma_Transformation.fig
%      MA_TRANSFORMATION, by itself, creates a new MA_TRANSFORMATION or raises the existing
%      singleton*.
%
%      H = MA_TRANSFORMATION returns the handle to a new MA_TRANSFORMATION or the handle to
%      the existing singleton*.
%
%      MA_TRANSFORMATION('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in MA_TRANSFORMATION.M with the given input arguments.
%
%      MA_TRANSFORMATION('Property','Value',...) creates a new MA_TRANSFORMATION or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before ma_Transformation_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to ma_Transformation_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help ma_Transformation

% Last Modified by GUIDE v2.5 17-Sep-2019 09:42:06

% Begin initialization code - DO NOT EDIT
gui_Singleton = 0;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @ma_Transformation_OpeningFcn, ...
                   'gui_OutputFcn',  @ma_Transformation_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before ma_Transformation is made visible.
function ma_Transformation_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to ma_Transformation (see VARARGIN)

% Choose default command line output for ma_Transformation
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes ma_Transformation wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = ma_Transformation_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% handles    structure with handles and user data (see GUIDATA)
set(gcf,'visible','off');
magnetic('visible','on');


function pushbutton2_Callback(hObject, eventdata, handles)
%% 读取数据
[filename,pathname]=uigetfile({'*.dat';'*.txt'},'请选择磁测数据文件');
if ~filename==0
    str=[pathname filename];
    data=dlmread(str);%读取文件数据 
    axes(handles.axes1)
    x=data(:,1);
    y=data(:,2);
    handles.xData=x;
    handles.yData=y;
    guidata(hObject,handles)
    plot(x,y,'-*b')
    grid on
    xlabel('\bf测点号')
    ylabel('\bf磁异常值ΔT/nT')
%     xyplot
    legend('原始数据')
    hold on
else 
    msgbox('打开文件失败','massage');
end


function pushbutton3_Callback(hObject, eventdata, handles)
%% 保存图片
    new_f_handle=figure('visible','off'); %新建一个不可见的figure
    new_axes=copyobj(handles.axes1,new_f_handle); %axes1是GUI界面内要保存图线的Tag，将其copy到不可见的figure中
    set(new_axes,'Units','normalized','Position',[0.1 0.1 0.8 0.8]);%将图线缩放，units是计量单位，position =  [x y width height]
    [filename, pathname ,fileindex]=uiputfile({'*.png';'*.bmp';'*.jpg';'*.eps';},'图片保存为');
    if  filename~=0%未点“取消”按钮或未关闭
        file=strcat(pathname,filename);
        switch fileindex %根据不同的选择保存为不同的类型        
            case 1
                print(new_f_handle,'-dpng',file);% print(new_f_handle,'-dpng',filename);效果一样，将图像打印到指定文件中
                msgbox({'图片保存成功：',pathname,filename},'提示')
%                 fprintf('>>已保存到：%s\n',file);
            case 2
                print(new_f_handle,'-dbmp',file);
                msgbox({'图片保存成功：',pathname,filename},'提示')
%                 fprintf('>>已保存到：%s\n',file);
            case 3
                print(new_f_handle,'-djpeg',file);
                msgbox({'图片保存成功：',pathname,filename},'提示')
%                 fprintf('>>已保存到：%s\n',file);
            case 4
                print(new_f_handle,'-depsc',file);
                msgbox({'图片保存成功：',pathname,filename},'提示')
%                 fprintf('>>已保存到：%s\n',file);
        end 
    else 
        msgbox('保存失败！','massage');    
    end


function pushbutton4_Callback(hObject, eventdata, handles)
x=handles.xData;
y=handles.yData;
yy=movmean(y,5);
handles.ySmooth=yy;
guidata(hObject,handles)
axes(handles.axes1)
plot(x,yy,'-*g')
legend('原始数据','圆滑数据')

function pushbutton15_Callback(hObject, eventdata, handles)
cla reset

function pushbutton12_Callback(hObject, eventdata, handles)
%% Za->Ha转换
set(gcf,'visible','off');
ma_Transformation_Conversion('visible','on');

function pushbutton11_Callback(hObject, eventdata, handles)


function pushbutton6_Callback(hObject, eventdata, handles)
%% 向下延拓



%% 


function pushbutton5_Callback(hObject, eventdata, handles)
%% 向上延拓
x=handles.xData;
y=handles.yData;
nx=size(y,1);
yy=zeros(1,nx);
n=2;
h=1;
for i=(h*n+1):(nx-h*n)
        tmp=0;
        for j=(i-h*n):h:(i+h*n)
            k=(j-i)/h;    
            tmp=tmp+y(j)*atan(8/(4*k*k+15))/pi;
        end
        yy(i)=tmp; 
end
plot(x(3:end-2),yy(3:end-2),'-*r')
legend('原始数据','圆滑数据','向上延拓')
    

function pushbutton19_Callback(hObject, eventdata, handles)


function pushbutton18_Callback(hObject, eventdata, handles)


function pushbutton17_Callback(hObject, eventdata, handles)


function pushbutton16_Callback(hObject, eventdata, handles)


function Untitled_1_Callback(hObject, eventdata, handles)


function Untitled_2_Callback(hObject, eventdata, handles)
%打开新界面
set(gcf,'visible','off')
matlab_task2_gui('visible','on');
