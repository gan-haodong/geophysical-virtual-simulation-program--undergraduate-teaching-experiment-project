function varargout = start_interface(varargin)
gui_Singleton = 0;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @start_interface_OpeningFcn, ...
                   'gui_OutputFcn',  @start_interface_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before start_interface is made visible.
function start_interface_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to start_interface (see VARARGIN)

% Choose default command line output for start_interface
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes start_interface wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = start_interface_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in magnetic.
function magnetic_Callback(hObject, eventdata, handles)
% hObject    handle to magnetic (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    set(gcf, 'Visible', 'off'); %隐藏主界面
    magnetic('Visible', 'on');



% --- Executes on button press in gravity.
function gravity_Callback(hObject, eventdata, handles)
% hObject    handle to gravity (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    set(gcf, 'Visible', 'off'); %隐藏主界面
   gravity('Visible', 'on');

% --- Executes on button press in seismic.
function seismic_Callback(hObject, eventdata, handles)
% hObject    handle to seismic (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    set(gcf, 'Visible', 'off'); %隐藏主界面
    seismic('Visible', 'on');

% --- Executes on button press in electrical.
function electrical_Callback(hObject, eventdata, handles)
% hObject    handle to electrical (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
 set(gcf, 'Visible', 'off'); %隐藏主界面
    electrical('Visible', 'on');


% --------------------------------------------------------------------
function file_Callback(hObject, eventdata, handles)
% hObject    handle to file (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function statement_Callback(hObject, eventdata, handles)
% hObject    handle to statement (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% h=msgbox('easy geophysics软件是一款教学仿真软件，一切功能只用于教学','使用说明');
%% 打开说明文档文件夹
winopen('软件使用说明书.pdf')
%% 

% --------------------------------------------------------------------
function help_Callback(hObject, eventdata, handles)
% hObject    handle to help (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
web https://lanzous.com/id1fdob
% --------------------------------------------------------------------
function about_Callback(hObject, eventdata, handles)
% hObject    handle to about (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
h=msgbox('欢迎使用本软件，软件开发者：甘浩东，使用中存在问题请发送信息至邮箱384482416@qq.com','关于软件');


% --------------------------------------------------------------------
function Untitled_1_Callback(hObject, eventdata, handles)
% hObject    handle to Untitled_1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
close;
