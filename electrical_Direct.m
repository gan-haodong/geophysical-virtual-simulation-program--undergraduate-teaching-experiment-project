function varargout = electrical_Direct(varargin)
% ELECTRICAL_DIRECT MATLAB code for electrical_Direct.fig
%      ELECTRICAL_DIRECT, by itself, creates a new ELECTRICAL_DIRECT or raises the existing
%      singleton*.
%
%      H = ELECTRICAL_DIRECT returns the handle to a new ELECTRICAL_DIRECT or the handle to
%      the existing singleton*.
%
%      ELECTRICAL_DIRECT('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in ELECTRICAL_DIRECT.M with the given input arguments.
%
%      ELECTRICAL_DIRECT('Property','Value',...) creates a new ELECTRICAL_DIRECT or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before electrical_Direct_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to electrical_Direct_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help electrical_Direct

% Last Modified by GUIDE v2.5 08-May-2020 21:15:37

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @electrical_Direct_OpeningFcn, ...
                   'gui_OutputFcn',  @electrical_Direct_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before electrical_Direct is made visible.
function electrical_Direct_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to electrical_Direct (see VARARGIN)

% Choose default command line output for electrical_Direct
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes electrical_Direct wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = electrical_Direct_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


function pushbutton4_Callback(hObject, eventdata, handles)
set(gcf,'visible','off')
set(electrical,'visible','on')

function pushbutton1_Callback(hObject, eventdata, handles)
set(gcf,'visible','off');
set(electrical_Direct_observe,'visible','on');

function pushbutton2_Callback(hObject, eventdata, handles)
set(gcf,'visible','off')
set(electrical_Direct_Principle,'visible','on')

function pushbutton3_Callback(hObject, eventdata, handles)
set(gcf,'visible','off')
set(matlab_rho_task3_gui,'visible','on')


function pushbutton5_Callback(hObject, eventdata, handles)
cmd = 'highDensity1.2.3\painterLine2.exe';
system(cmd);
