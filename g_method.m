function varargout = g_method(varargin)
% G_METHOD MATLAB code for g_method.fig
%      G_METHOD, by itself, creates a new G_METHOD or raises the existing
%      singleton*.
%
%      H = G_METHOD returns the handle to a new G_METHOD or the handle to
%      the existing singleton*.
%
%      G_METHOD('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in G_METHOD.M with the given input arguments.
%
%      G_METHOD('Property','Value',...) creates a new G_METHOD or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before g_method_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to g_method_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help g_method

% Last Modified by GUIDE v2.5 29-Aug-2019 09:34:07

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @g_method_OpeningFcn, ...
                   'gui_OutputFcn',  @g_method_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before g_method is made visible.
function g_method_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to g_method (see VARARGIN)

% Choose default command line output for g_method
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes g_method wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = g_method_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(gcf,'visible','off');
gravity('visible','on');
