function varargout = gravity(varargin)
% GRAVITY MATLAB code for gravity.fig
%      GRAVITY, by itself, creates a new GRAVITY or raises the existing
%      singleton*.
%
%      H = GRAVITY returns the handle to a new GRAVITY or the handle to
%      the existing singleton*.
%
%      GRAVITY('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in GRAVITY.M with the given input arguments.
%
%      GRAVITY('Property','Value',...) creates a new GRAVITY or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before gravity_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to gravity_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help gravity

% Last Modified by GUIDE v2.5 02-Sep-2019 23:27:50

% Begin initialization code - DO NOT EDIT
gui_Singleton = 0;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @gravity_OpeningFcn, ...
                   'gui_OutputFcn',  @gravity_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before gravity is made visible.
function gravity_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to gravity (see VARARGIN)

% Choose default command line output for gravity
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes gravity wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = gravity_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;



function edit1_Callback(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit1 as text
%        str2double(get(hObject,'String')) returns contents of edit1 as a double


% --- Executes during object creation, after setting all properties.
function edit1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(gcf,'visible','off');
start_interface('visible','on');


% --- Executes on button press in g_display.
function g_display_Callback(hObject, eventdata, handles)
% hObject    handle to g_display (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(gcf,'visible','off');
g_display('visible','on');

% --- Executes on button press in g_method.
function g_method_Callback(hObject, eventdata, handles)
% hObject    handle to g_method (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(gcf,'visible','off');
g_method('visible','on');

% --- Executes on button press in g_forward.
function g_forward_Callback(hObject, eventdata, handles)
% hObject    handle to g_forward (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(gcf,'visible','off');
g_forward('visible','on');

% --- Executes on button press in g_inversion.
function g_inversion_Callback(hObject, eventdata, handles)
% hObject    handle to g_inversion (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(gcf,'visible','off');
g_Transformation('visible','on');

% --- Executes on button press in g_application.
function g_application_Callback(hObject, eventdata, handles)
% hObject    handle to g_application (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(gcf,'visible','off');
g_application('visible','on');
