function varargout = matlab_rho_task3_gui(varargin)
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @matlab_rho_task3_gui_OpeningFcn, ...
                   'gui_OutputFcn',  @matlab_rho_task3_gui_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
function matlab_rho_task3_gui_OpeningFcn(hObject, eventdata, handles, varargin)
handles.output = hObject;
guidata(hObject, handles);

function varargout = matlab_rho_task3_gui_OutputFcn(hObject, eventdata, handles) 
varargout{1} = handles.output;

function pushbutton1_Callback(hObject, eventdata, handles)
rho=str2num(get(handles.R,'string'));
h=str2num(get(handles.H,'string'));
for i=2:length(h)
    h(i)=h(i-1)+h(i);
end
h=-h;
n=length(h);
axes(handles.axes1);
for i=1:n
    plot3([0 10],[0 0],[h(i) h(i)],'-r')
    xlabel('X')
    ylabel('Y')
    zlabel('H')
    grid on
    hold on
    plot3([0 0],[0 10],[h(i) h(i)],'-r')
    hold on
    plot3([0 10],[10 10],[h(i) h(i)],'-r')
    hold on
    plot3([10 10],[0 10],[h(i) h(i)],'-r')
    hold on
    text(0,10,h(i)+50,['\leftarrow','\rho(',num2str(i),')=',num2str(rho(i))])
end
plot3([0 10],[0 0],[0 0],'-r')
xlabel('X')
ylabel('Y')
zlabel('H')
title('分层介质示意图')
grid on
hold on
plot3([0 0],[0 10],[0 0],'-r')
hold on
plot3([0 10],[10 10],[0 0],'-r')
hold on
plot3([10 10],[0 10],[0 0],'-r')
hold on
plot3([0 0],[10 10],[h(end) h(end)-100],'-r')
%画出竖直边线
plot3([0 0],[0 0],[0 h(end)-100],'-r')
plot3([10 10],[0 0],[0 h(end)-100],'-r')
plot3([10 10],[10 10],[0 h(end)-100],'-r')
plot3([0 0],[10 10],[0 h(end)-100],'-r')
text(0,10,h(end)-50,['\leftarrow','\rho(',num2str(n+1),')=',num2str(rho(end))])

function pushbutton2_Callback(hObject, eventdata, handles)
n=str2num(get(handles.num,'string'));
dx=0.2;
rho =str2num(get(handles.R,'string'));
h =str2num(get(handles.H,'string'));
if (size(rho,2)-size(h,2))~=1 || n~=size(rho,2)
    msgbox('请检查参数设置！！！');
else
    rm = logspace(0,5,60);
    Um =DC1D_Forward(rho,h,rm);
    rn = rm+dx;
    Un =DC1D_Forward(rho,h,rn);
    axes(handles.axes2)
    %二极装置
    kam = 2*3.1415926.*rm;
    rho_am  = kam.*Um;
    semilogx(rm,rho_am,'r+-');hold on;
    %对称四极装置
    kamnb =3.1415926.*rm.*rn./(rn-rm);
    Umm=Um-Un; Unn=-Um+Un;
    rho_amnb = kamnb.*(Umm-Unn);
    semilogx((rm+rn)/2,rho_amnb,'kd-');hold on;
    %偶极偶极装置
    kabmn =2*3.1415926.*rm.*rn.*(rm-dx).*(rn-dx)./(rn-rm)...
        ./(rm.*rn-(rm-dx).*(rn-dx)); 
    rbm =rm-dx;rbn =rm;
    Ubm =DC1D_Forward(rho,h,rbm);
    Ummm =Um-Ubm; Unnn =Un-Um;
    rho_abmn = abs(kabmn.*(Ummm-Unnn));
    semilogx((rm+rn)/2,rho_abmn,'go-');
    hold on;
    xlabel('x/m');ylabel('ρ_s/Ω.m');
    title('直流电测深视电阻率曲线')
    grid on
    legend('二极装置','对称四极装置','偶极-偶极装置','location','best');
end

function pushbutton3_Callback(hObject, eventdata, handles)
num=str2double(get(handles.num,'string'));
rho=[200 300 100 300];
h=[100 100 100];
for i=1:num
    rho1(i)=rho(i);
end
for i=1:num-1
    h1(i)=h(i);
end
set(handles.uitable1,'data',rho1)
set(handles.uitable2,'data',h1)

function pushbutton4_Callback(hObject, eventdata, handles)
axes(handles.axes1)
cla reset
axes(handles.axes2)
cla reset

function uitable1_CellSelectionCallback(hObject, eventdata, handles)
function pushbutton5_Callback(hObject, eventdata, handles)
function num_Callback(hObject, eventdata, handles)
function num_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function pushbutton6_Callback(hObject, eventdata, handles)
function pushbutton7_Callback(hObject, eventdata, handles)
set(gcf,'visible','off')
set(user_instruction,'visible','on')


function R_Callback(hObject, eventdata, handles)


function R_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function H_Callback(hObject, eventdata, handles)


function H_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function pushbutton8_Callback(hObject, eventdata, handles)
set(gcf,'visible','off')
set(electrical_Direct,'visible','on')


function pushbutton9_Callback(hObject, eventdata, handles)
%% 保存图片
    new_f_handle=figure('visible','off'); %新建一个不可见的figure
    new_axes=copyobj(handles.axes2,new_f_handle); %axes1是GUI界面内要保存图线的Tag，将其copy到不可见的figure中
    set(new_axes,'Units','normalized','Position',[0.1 0.1 0.8 0.8]);%将图线缩放，units是计量单位，position =  [x y width height]
    [filename, pathname ,fileindex]=uiputfile({'*.png';'*.bmp';'*.jpg';'*.eps';},'图片保存为');
    if  filename~=0%未点“取消”按钮或未关闭
        file=strcat(pathname,filename);
        switch fileindex %根据不同的选择保存为不同的类型        
            case 1
                print(new_f_handle,'-dpng',file);% print(new_f_handle,'-dpng',filename);效果一样，将图像打印到指定文件中
                msgbox({'图片保存成功：',pathname,filename},'提示')
%                 fprintf('>>已保存到：%s\n',file);
            case 2
                print(new_f_handle,'-dbmp',file);
                msgbox({'图片保存成功：',pathname,filename},'提示')
%                 fprintf('>>已保存到：%s\n',file);
            case 3
                print(new_f_handle,'-djpeg',file);
                msgbox({'图片保存成功：',pathname,filename},'提示')
%                 fprintf('>>已保存到：%s\n',file);
            case 4
                print(new_f_handle,'-depsc',file);
                msgbox({'图片保存成功：',pathname,filename},'提示')
%                 fprintf('>>已保存到：%s\n',file);
        end 
    else 
        msgbox('保存失败！','massage');    
    end
