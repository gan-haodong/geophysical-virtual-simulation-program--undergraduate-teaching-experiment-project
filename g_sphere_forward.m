function varargout = g_sphere_forward(varargin)
% G_SPHERE_FORWARD MATLAB code for g_sphere_forward.fig
%      G_SPHERE_FORWARD, by itself, creates a new G_SPHERE_FORWARD or raises the existing
%      singleton*.
%
%      H = G_SPHERE_FORWARD returns the handle to a new G_SPHERE_FORWARD or the handle to
%      the existing singleton*.
%
%      G_SPHERE_FORWARD('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in G_SPHERE_FORWARD.M with the given input arguments.
%
%      G_SPHERE_FORWARD('Property','Value',...) creates a new G_SPHERE_FORWARD or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before g_sphere_forward_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to g_sphere_forward_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help g_sphere_forward

% Last Modified by GUIDE v2.5 09-Sep-2019 00:11:15

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @g_sphere_forward_OpeningFcn, ...
                   'gui_OutputFcn',  @g_sphere_forward_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before g_sphere_forward is made visible.
function g_sphere_forward_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to g_sphere_forward (see VARARGIN)

% Choose default command line output for g_sphere_forward
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes g_sphere_forward wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = g_sphere_forward_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


function pushbutton12_Callback(hObject, eventdata, handles)
 new_f_handle=figure('visible','off'); %新建一个不可见的figure
    new_axes=copyobj(handles.axes1,new_f_handle); %axes2是GUI界面内要保存图线的Tag，将其copy到不可见的figure中
    set(new_axes,'Units','normalized','Position',[0.1 0.1 0.8 0.8]);%将图线缩放
    [filename, pathname ,fileindex]=uiputfile({'*.png';'*.bmp';'*.jpg';'*.eps';},'图片保存为');
    if  filename~=0%未点“取消”按钮或未关闭
        file=strcat(pathname,filename);
        switch fileindex %根据不同的选择保存为不同的类型        
            case 1
                print(new_f_handle,'-dpng',file);% print(new_f_handle,'-dpng',filename);效果一样，将图像打印到指定文件中
                msgbox({'图片保存成功：',pathname,filename},'提示')
%                 fprintf('>>已保存到：%s\n',file);
            case 2
                print(new_f_handle,'-dbmp',file);
                msgbox({'图片保存成功：',pathname,filename},'提示')
%                 fprintf('>>已保存到：%s\n',file);
            case 3
                print(new_f_handle,'-djpeg',file);
                msgbox({'图片保存成功：',pathname,filename},'提示')
%                 fprintf('>>已保存到：%s\n',file);
            case 4
                print(new_f_handle,'-depsc',file);
                msgbox({'图片保存成功：',pathname,filename},'提示')
%                 fprintf('>>已保存到：%s\n',file);
        end 
    else 
        msgbox('保存失败！','massage');    
    end

function pushbutton13_Callback(hObject, eventdata, handles)
global start
start=[0 0 0];

function pushbutton14_Callback(hObject, eventdata, handles)
global start
start=[0 0 0];
set(gcf,'visible','off');
g_forward('visible','on');

function popupmenu1_Callback(hObject, eventdata, handles)
sel_2=get(hObject,'value');
handles.sel_2=sel_2;
guidata(hObject,handles);

function popupmenu1_CreateFcn(hObject, eventdata, handles)

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function time_Callback(hObject, eventdata, handles)


function time_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
global time
time=0.5;

function pushbutton7_Callback(hObject, eventdata, handles)
global time
time=str2double(get(handles.time,'string'));

function R_Callback(hObject, eventdata, handles)


function R_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function H_Callback(hObject, eventdata, handles)


function H_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function delta_p_Callback(hObject, eventdata, handles)


function delta_p_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function popupmenu2_Callback(hObject, eventdata, handles)
sel_1=get(hObject,'value');
handles.sel_1=sel_1;
guidata(hObject,handles);

function popupmenu2_CreateFcn(hObject, eventdata, handles)

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function pushbutton9_Callback(hObject, eventdata, handles)
%% 球体半径
% R1=str2double(get(handles.R1,'string'));
% H1=str2double(get(handles.H1,'string'));
% ro1=str2double(get(handles.ro1,'string'));
% dxdy1=str2double(get(handles.dxdy1,'string'));
% min1=str2double(get(handles.min1,'string'));
%预处理
% set(handles.R,'string','50');
global start 
global time
start=[1 0 0];
set(handles.H,'string','100');
set(handles.delta_p,'string','1');
min1=-500;
H=100;
delta_p=1;
d=5;
%% 计算
G=6.672*10^(-11);
x=[min1,abs(min1)];
y=x;
N=(x(2)-x(1))/d+1;
M=(y(2)-y(1))/d+1;
axes(handles.axes1)
for R=10:5:100
    if start(1)
      delta_m=4/3*pi*delta_p*R^3;
      for i=1:N
        X=x(1)+d*(i-1);
        for j=1:M
            Y=y(1)+d*(j-1);
            r=sqrt(X^2+Y^2+H^2);
            v_xx(i,j)=(2*X^2-Y^2-H^2)/r^5;
            v_yy(i,j)=(2*Y^2-X^2-H^2)/r^5;
            v_zz(i,j)=(2*H^2-X^2-Y^2)/r^5;
            v_xy(i,j)=3*Y*X/r^5;
            v_xz(i,j)=-3*H*X/r^5;
            v_yz(i,j)=-3*H*Y/r^5;  
            v_zzz(i,j)=3*(2*H^2-3*X^2-Y^2)/r^7;
        end
      end
      V_xz=v_xz*G*delta_m;
      V_xy=v_xy*G*delta_m;
      V_zz=v_zz*G*delta_m;
      V_yy=v_yy*G*delta_m;
      V_yz=v_yz*G*delta_m;
      V_xx=v_xx*G*delta_m;
      V_zzz=v_zzz*G*delta_m;
      x1=linspace(x(1),x(2),N);
      y1=linspace(y(1),y(2),M);
      switch handles.sel_1
        case 1
        case 2
            switch handles.sel_2
                case 1
                case 2
                    surfc(x1,y1,V_xx);
                    xlabel('\bfX/m','rotation',30)
                    ylabel('\bfY/m','rotation',-45)
                    zlabel('\bfVxx/E')
                    
                    title('\bf球体重力异常梯度Vxx/E','fontsize',15)
                    shading flat
                    set(handles.R,'string',num2str(R))
                    colormap summer
                    colorbar
                case 3
                    contourf(x1,y1,V_xx); 
                    xlabel('\bfX/m')
                    ylabel('\bfY/m')
                    colorbar
                    set(handles.R,'string',num2str(R))
                    colormap summer
                    title('\bf球体重力异常梯度Vxx/E','fontsize',15)
            end
        case 3
            switch handles.sel_2
                case 1
                case 2
                    surfc(x1,y1,V_yy);
                    xlabel('\bfX/m','rotation',30)
                    ylabel('\bfY/m','rotation',-45)
                    zlabel('\bfVyy/E')
                    title('\bf球体重力异常梯度Vyy/E','fontsize',15)
                    shading flat
                    set(handles.R,'string',num2str(R))
                    colormap summer
                    colorbar
                case 3
                    contourf(x1,y1,V_yy); 
                    xlabel('\bfX/m')
                    ylabel('\bfY/m')
                    colorbar
                    colormap summer
                    set(handles.R,'string',num2str(R))
                    title('\bf球体重力异常梯度Vyy/E','fontsize',15)
            end
        case 4
            switch handles.sel_2
                case 1
                case 2
                    surfc(x1,y1,V_zz);
                    xlabel('\bfX/m','rotation',30)
                    ylabel('\bfY/m','rotation',-45)
                    zlabel('\bfVzz/E')
                    title('\bf球体重力异常梯度Vzz/E','fontsize',15)
                    shading flat
                    set(handles.R,'string',num2str(R))
                    colormap summer
                    colorbar
                case 3
                    contourf(x1,y1,V_zz); 
                    xlabel('\bfX/m')
                    ylabel('\bfY/m')
                    colorbar
                    set(handles.R,'string',num2str(R))
                    colormap summer
                    title('\bf球体重力异常梯度Vzz/E','fontsize',15)
            end
        case 5
            switch handles.sel_2
                case 1
                case 2
                    surfc(x1,y1,V_xy);
                    xlabel('\bfX/m','rotation',30)
                    ylabel('\bfY/m','rotation',-45)
                    zlabel('\bfVxx/E')
                    title('\bf球体重力异常梯度Vxy/E','fontsize',15)
                    shading flat
                    set(handles.R,'string',num2str(R))
                    colormap summer
                    colorbar
                case 3
                    contourf(x1,y1,V_xy); 
                    xlabel('\bfX/m')
                    ylabel('\bfY/m')
                    colorbar
                    colormap summer
                    set(handles.R,'string',num2str(R))
                    title('\bf球体重力异常梯度Vxy/E','fontsize',15)
            end
        case 6
            switch handles.sel_2
                case 1
                case 2
                    surfc(x1,y1,V_xz);
                    xlabel('\bfX/m','rotation',30)
                    ylabel('\bfY/m','rotation',-45)
                    zlabel('\bfVxz/E')
                    title('\bf球体重力异常梯度Vxz/E','fontsize',15)
                    shading flat
                    set(handles.R,'string',num2str(R))
                    colormap summer
                    colorbar
                case 3
                    contourf(x1,y1,V_xz); 
                    xlabel('\bfX/m')
                    ylabel('\bfY/m')
                    colorbar
                    set(handles.R,'string',num2str(R))
                    colormap summer
                    title('\bf球体重力异常梯度Vxz/E','fontsize',15)
            end
        case 7
            switch handles.sel_2
                case 1
                case 2
                    surfc(x1,y1,V_yz);
                    xlabel('\bfX/m','rotation',30)
                    ylabel('\bfY/m','rotation',-45)
                    zlabel('\bfVyz/E')
                    title('\bf球体重力异常梯度Vyz/E','fontsize',15)
                    shading flat
                    set(handles.R,'string',num2str(R))
                    colormap summer
                    colorbar
                case 3
                    contourf(x1,y1,V_yz); 
                    xlabel('\bfX/m')
                    ylabel('\bfY/m')
                    set(handles.R,'string',num2str(R))
                    colorbar
                    colormap summer
                    title('\bf球体重力异常梯度Vyz/E','fontsize',15)
            end
        case 8
            switch handles.sel_2
                case 1
                case 2
                    surfc(x1,y1,V_zzz);
                    xlabel('\bfX/m','rotation',30)
                    ylabel('\bfY/m','rotation',-45)
                    zlabel('\bfVzzz/nMKS')
                    title('\bf球体重力异常梯度Vzzz/nMKS','fontsize',15)
                    shading flat
                    set(handles.R,'string',num2str(R))
                    colormap summer
                    colorbar
                case 3
                    contourf(x1,y1,V_zzz); 
                    set(handles.R,'string',num2str(R))
                    xlabel('\bfX/m')
                    ylabel('\bfY/m')
                    colorbar
                    colormap summer
                    title('\bf球体重力异常梯度Vzzz/nMKS','fontsize',15)
            end
      end
     
    end
    pause(time);
end

%% 


function pushbutton10_Callback(hObject, eventdata, handles)
%% 埋深
global start 
global time
start=[0 1 0];
set(handles.R,'string','50');
set(handles.delta_p,'string','1');
min1=-500;
R=50;
delta_p=1;
d=5;

delta_m=4/3*pi*delta_p*R^3;
%% 计算
G=6.672*10^(-11);
x=[min1,abs(min1)];
y=x;
N=(x(2)-x(1))/d+1;
M=(y(2)-y(1))/d+1;
axes(handles.axes1)
for H=50:10:200
    if start(2)
      
      for i=1:N
        X=x(1)+d*(i-1);
        for j=1:M
            Y=y(1)+d*(j-1);
            r=sqrt(X^2+Y^2+H^2);
            v_xx(i,j)=(2*X^2-Y^2-H^2)/r^5;
            v_yy(i,j)=(2*Y^2-X^2-H^2)/r^5;
            v_zz(i,j)=(2*H^2-X^2-Y^2)/r^5;
            v_xy(i,j)=3*Y*X/r^5;
            v_xz(i,j)=-3*H*X/r^5;
            v_yz(i,j)=-3*H*Y/r^5;  
            v_zzz(i,j)=3*(2*H^2-3*X^2-Y^2)/r^7;
        end
      end
      V_xz=v_xz*G*delta_m;
      V_xy=v_xy*G*delta_m;
      V_zz=v_zz*G*delta_m;
      V_yy=v_yy*G*delta_m;
      V_yz=v_yz*G*delta_m;
      V_xx=v_xx*G*delta_m;
      V_zzz=v_zzz*G*delta_m;
      x1=linspace(x(1),x(2),N);
      y1=linspace(y(1),y(2),M);
      switch handles.sel_1
        case 1
        case 2
            switch handles.sel_2
                case 1
                case 2
                    surfc(x1,y1,V_xx');
                    xlabel('\bfX/m','rotation',30)
                    ylabel('\bfY/m','rotation',-45)
                    zlabel('\bfVxx/E')
                    
                    title('\bf球体重力异常梯度Vxx/E','fontsize',15)
                    shading flat
                    set(handles.H,'string',num2str(H))
                    colormap summer
                    colorbar
                case 3
                    contourf(x1,y1,V_xx'); 
                    xlabel('\bfX/m')
                    ylabel('\bfY/m')
                    colorbar
                    set(handles.H,'string',num2str(H))
                    colormap summer
                    title('\bf球体重力异常梯度Vxx/E','fontsize',15)
            end
        case 3
            switch handles.sel_2
                case 1
                case 2
                    surfc(x1,y1,V_yy');
                    xlabel('\bfX/m','rotation',30)
                    ylabel('\bfY/m','rotation',-45)
                    zlabel('\bfVyy/E')
                    title('\bf球体重力异常梯度Vyy/E','fontsize',15)
                    shading flat
                    set(handles.H,'string',num2str(H))
                    colormap summer
                    colorbar
                case 3
                    contourf(x1,y1,V_yy'); 
                    xlabel('\bfX/m')
                    ylabel('\bfY/m')
                    colorbar
                    colormap summer
                    set(handles.H,'string',num2str(H))
                    title('\bf球体重力异常梯度Vyy/E','fontsize',15)
            end
        case 4
            switch handles.sel_2
                case 1
                case 2
                    surfc(x1,y1,V_zz');
                    xlabel('\bfX/m','rotation',30)
                    ylabel('\bfY/m','rotation',-45)
                    zlabel('\bfVzz/E')
                    title('\bf球体重力异常梯度Vzz/E','fontsize',15)
                    shading flat
                    set(handles.H,'string',num2str(H))
                    colormap summer
                    colorbar
                case 3
                    contourf(x1,y1,V_zz'); 
                    xlabel('\bfX/m')
                    ylabel('\bfY/m')
                    colorbar
                    set(handles.H,'string',num2str(H))
                    colormap summer
                    title('\bf球体重力异常梯度Vzz/E','fontsize',15)
            end
        case 5
            switch handles.sel_2
                case 1
                case 2
                    surfc(x1,y1,V_xy');
                    xlabel('\bfX/m','rotation',30)
                    ylabel('\bfY/m','rotation',-45)
                    zlabel('\bfVxx/E')
                    title('\bf球体重力异常梯度Vxy/E','fontsize',15)
                    shading flat
                    set(handles.H,'string',num2str(H))
                    colormap summer
                    colorbar
                case 3
                    contourf(x1,y1,V_xy'); 
                    xlabel('\bfX/m')
                    ylabel('\bfY/m')
                    colorbar
                    colormap summer
                    set(handles.H,'string',num2str(H))
                    title('\bf球体重力异常梯度Vxy/E','fontsize',15)
            end
        case 6
            switch handles.sel_2
                case 1
                case 2
                    surfc(x1,y1,V_xz');
                    xlabel('\bfX/m','rotation',30)
                    ylabel('\bfY/m','rotation',-45)
                    zlabel('\bfVxz/E')
                    title('\bf球体重力异常梯度Vxz/E','fontsize',15)
                    shading flat
                    set(handles.H,'string',num2str(H))
                    colormap summer
                    colorbar
                case 3
                    contourf(x1,y1,V_xz'); 
                    xlabel('\bfX/m')
                    ylabel('\bfY/m')
                    colorbar
                    set(handles.H,'string',num2str(H))
                    colormap summer
                    title('\bf球体重力异常梯度Vxz/E','fontsize',15)
            end
        case 7
            switch handles.sel_2
                case 1
                case 2
                    surfc(x1,y1,V_yz');
                    xlabel('\bfX/m','rotation',30)
                    ylabel('\bfY/m','rotation',-45)
                    zlabel('\bfVyz/E')
                    title('\bf球体重力异常梯度Vyz/E','fontsize',15)
                    shading flat
                    set(handles.H,'string',num2str(H))
                    colormap summer
                    colorbar
                case 3
                    contourf(x1,y1,V_yz'); 
                    xlabel('\bfX/m')
                    ylabel('\bfY/m')
                    set(handles.H,'string',num2str(H))
                    colorbar
                    colormap summer
                    title('\bf球体重力异常梯度Vyz/E','fontsize',15)
            end
        case 8
            switch handles.sel_2
                case 1
                case 2
                    surfc(x1,y1,V_zzz');
                    xlabel('\bfX/m','rotation',30)
                    ylabel('\bfY/m','rotation',-45)
                    zlabel('\bfVzzz/nMKS')
                    title('\bf球体重力异常梯度Vzzz/nMKS','fontsize',15)
                    shading flat
                    set(handles.H,'string',num2str(H))
                    colormap summer
                    colorbar
                case 3
                    contourf(x1,y1,V_zzz'); 
                    set(handles.H,'string',num2str(H))
                    xlabel('\bfX/m')
                    ylabel('\bfY/m')
                    colorbar
                    colormap summer
                    title('\bf球体重力异常梯度Vzzz/nMKS','fontsize',15)
            end
      end
     
    end
    pause(time);
end

%% 


function pushbutton11_Callback(hObject, eventdata, handles)
%% 剩余密度
global start 
global time
start=[0 0 1];
set(handles.R,'string','50');
set(handles.H,'string','100');
min1=-500;
R=50;
H=100;
d=5;


%% 计算
G=6.672*10^(-11);
x=[min1,abs(min1)];
y=x;
N=(x(2)-x(1))/d+1;
M=(y(2)-y(1))/d+1;
axes(handles.axes1)
for delta_p=0.5:0.5:5
    if start(3)
      delta_m=4/3*pi*delta_p*R^3;
      for i=1:N
        X=x(1)+d*(i-1);
        for j=1:M
            Y=y(1)+d*(j-1);
            r=sqrt(X^2+Y^2+H^2);
            v_xx(i,j)=(2*X^2-Y^2-H^2)/r^5;
            v_yy(i,j)=(2*Y^2-X^2-H^2)/r^5;
            v_zz(i,j)=(2*H^2-X^2-Y^2)/r^5;
            v_xy(i,j)=3*Y*X/r^5;
            v_xz(i,j)=-3*H*X/r^5;
            v_yz(i,j)=-3*H*Y/r^5;  
            v_zzz(i,j)=3*(2*H^2-3*X^2-Y^2)/r^7;
        end
      end
      V_xz=v_xz*G*delta_m;
      V_xy=v_xy*G*delta_m;
      V_zz=v_zz*G*delta_m;
      V_yy=v_yy*G*delta_m;
      V_yz=v_yz*G*delta_m;
      V_xx=v_xx*G*delta_m;
      V_zzz=v_zzz*G*delta_m;
      x1=linspace(x(1),x(2),N);
      y1=linspace(y(1),y(2),M);
      switch handles.sel_1
        case 1
        case 2
            switch handles.sel_2
                case 1
                case 2
                    surfc(x1,y1,V_xx');
                    xlabel('\bfX/m','rotation',30)
                    ylabel('\bfY/m','rotation',-45)
                    zlabel('\bfVxx/E')
                    
                    title('\bf球体重力异常梯度Vxx/E','fontsize',15)
                    shading flat
                    set(handles.delta_p,'string',num2str(delta_p))
                    colormap summer
                    colorbar
                case 3
                    contourf(x1,y1,V_xx'); 
                    xlabel('\bfX/m')
                    ylabel('\bfY/m')
                    colorbar
                    set(handles.delta_p,'string',num2str(delta_p))
                    colormap summer
                    title('\bf球体重力异常梯度Vxx/E','fontsize',15)
            end
        case 3
            switch handles.sel_2
                case 1
                case 2
                    surfc(x1,y1,V_yy');
                    xlabel('\bfX/m','rotation',30)
                    ylabel('\bfY/m','rotation',-45)
                    zlabel('\bfVyy/E')
                    title('\bf球体重力异常梯度Vyy/E','fontsize',15)
                    shading flat
                    set(handles.delta_p,'string',num2str(delta_p))
                    colormap summer
                    colorbar
                case 3
                    contourf(x1,y1,V_yy'); 
                    xlabel('\bfX/m')
                    ylabel('\bfY/m')
                    colorbar
                    colormap summer
                    set(handles.delta_p,'string',num2str(delta_p))
                    title('\bf球体重力异常梯度Vyy/E','fontsize',15)
            end
        case 4
            switch handles.sel_2
                case 1
                case 2
                    surfc(x1,y1,V_zz');
                    xlabel('\bfX/m','rotation',30)
                    ylabel('\bfY/m','rotation',-45)
                    zlabel('\bfVzz/E')
                    title('\bf球体重力异常梯度Vzz/E','fontsize',15)
                    shading flat
                    set(handles.delta_p,'string',num2str(delta_p))
                    colormap summer
                    colorbar
                case 3
                    contourf(x1,y1,V_zz'); 
                    xlabel('\bfX/m')
                    ylabel('\bfY/m')
                    colorbar
                    set(handles.delta_p,'string',num2str(delta_p))
                    colormap summer
                    title('\bf球体重力异常梯度Vzz/E','fontsize',15)
            end
        case 5
            switch handles.sel_2
                case 1
                case 2
                    surfc(x1,y1,V_xy');
                    xlabel('\bfX/m','rotation',30)
                    ylabel('\bfY/m','rotation',-45)
                    zlabel('\bfVxx/E')
                    title('\bf球体重力异常梯度Vxy/E','fontsize',15)
                    shading flat
                    set(handles.delta_p,'string',num2str(delta_p))
                    colormap summer
                    colorbar
                case 3
                    contourf(x1,y1,V_xy'); 
                    xlabel('\bfX/m')
                    ylabel('\bfY/m')
                    colorbar
                    colormap summer
                    set(handles.delta_p,'string',num2str(delta_p))
                    title('\bf球体重力异常梯度Vxy/E','fontsize',15)
            end
        case 6
            switch handles.sel_2
                case 1
                case 2
                    surfc(x1,y1,V_xz');
                    xlabel('\bfX/m','rotation',30)
                    ylabel('\bfY/m','rotation',-45)
                    zlabel('\bfVxz/E')
                    title('\bf球体重力异常梯度Vxz/E','fontsize',15)
                    shading flat
                    set(handles.delta_p,'string',num2str(delta_p))
                    colormap summer
                    colorbar
                case 3
                    contourf(x1,y1,V_xz'); 
                    xlabel('\bfX/m')
                    ylabel('\bfY/m')
                    colorbar
                    set(handles.delta_p,'string',num2str(delta_p))
                    colormap summer
                    title('\bf球体重力异常梯度Vxz/E','fontsize',15)
            end
        case 7
            switch handles.sel_2
                case 1
                case 2
                    surfc(x1,y1,V_yz');
                    xlabel('\bfX/m','rotation',30)
                    ylabel('\bfY/m','rotation',-45)
                    zlabel('\bfVyz/E')
                    title('\bf球体重力异常梯度Vyz/E','fontsize',15)
                    shading flat
                    set(handles.delta_p,'string',num2str(delta_p))
                    colormap summer
                    colorbar
                case 3
                    contourf(x1,y1,V_yz'); 
                    xlabel('\bfX/m')
                    ylabel('\bfY/m')
                    set(handles.delta_p,'string',num2str(delta_p))
                    colorbar
                    colormap summer
                    title('\bf球体重力异常梯度Vyz/E','fontsize',15)
            end
        case 8
            switch handles.sel_2
                case 1
                case 2
                    surfc(x1,y1,V_zzz');
                    xlabel('\bfX/m','rotation',30)
                    ylabel('\bfY/m','rotation',-45)
                    zlabel('\bfVzzz/nMKS')
                    title('\bf球体重力异常梯度Vzzz/nMKS','fontsize',15)
                    shading flat
                    set(handles.delta_p,'string',num2str(delta_p))
                    colormap summer
                    colorbar
                case 3
                    contourf(x1,y1,V_zzz'); 
                    set(handles.delta_p,'string',num2str(delta_p))
                    xlabel('\bfX/m')
                    ylabel('\bfY/m')
                    colorbar
                    colormap summer
                    title('\bf球体重力异常梯度Vzzz/nMKS','fontsize',15)
            end
      end
     
    end
    pause(time);
end

%% 
