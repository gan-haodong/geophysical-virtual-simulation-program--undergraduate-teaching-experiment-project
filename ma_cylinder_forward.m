function varargout = ma_cylinder_forward(varargin)
% MA_CYLINDER_FORWARD MATLAB code for ma_cylinder_forward.fig
%      MA_CYLINDER_FORWARD, by itself, creates a new MA_CYLINDER_FORWARD or raises the existing
%      singleton*.
%
%      H = MA_CYLINDER_FORWARD returns the handle to a new MA_CYLINDER_FORWARD or the handle to
%      the existing singleton*.
%
%      MA_CYLINDER_FORWARD('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in MA_CYLINDER_FORWARD.M with the given input arguments.
%
%      MA_CYLINDER_FORWARD('Property','Value',...) creates a new MA_CYLINDER_FORWARD or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before ma_cylinder_forward_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to ma_cylinder_forward_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help ma_cylinder_forward

% Last Modified by GUIDE v2.5 01-Sep-2019 12:15:57

% Begin initialization code - DO NOT EDIT
gui_Singleton = 0;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @ma_cylinder_forward_OpeningFcn, ...
                   'gui_OutputFcn',  @ma_cylinder_forward_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before ma_cylinder_forward is made visible.
function ma_cylinder_forward_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to ma_cylinder_forward (see VARARGIN)

% Choose default command line output for ma_cylinder_forward
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes ma_cylinder_forward wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = ma_cylinder_forward_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


function pushbutton1_Callback(hObject, eventdata, handles)


function pushbutton2_Callback(hObject, eventdata, handles)
%% 返回按钮

global start;
start=[0 0 0 0 0 0];
set(gcf,'visible','off')
ma_forward('visible','on')


function edit1_Callback(hObject, eventdata, handles)


function edit1_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function pushbutton3_Callback(hObject, eventdata, handles)


function popupmenu1_Callback(hObject, eventdata, handles)
sel=get(hObject,'value');
handles.sel=sel;
guidata(hObject,handles);

function popupmenu1_CreateFcn(hObject, eventdata, handles)

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function R_Callback(hObject, eventdata, handles)


function R_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

global time
time=0.5;
function pushbutton4_Callback(hObject, eventdata, handles)
%% 半径

global start;
start=[1 0 0 0 0 0];
global time;
set(handles.D,'string','30');
set(handles.k,'string','0.2');
set(handles.i,'string','0');
set(handles.a,'string','0');
set(handles.T,'string','50000');
sel=handles.sel;
% 测点分布范围
dx=2; % X方向测点间距
dy=2; % Y方向测点间距
nx=201; % X方向测点数
ny=201; % Y方向测点数
xmin=-200; % X方向起点
ymin=-200; % Y方向起点
x=xmin:dx:(xmin+(nx-1)*dx); % X方向范围
y=ymin:dy:(ymin+(ny-1)*dy); % Y方向范围
[X,Y]=meshgrid(x,y); % 转化为排列

u=4*pi*10^(-7);  %磁导率
i=0;  %磁化倾角I
A=0; %剖面磁方位角
T=50000;%地磁场T=50000nT
axes(handles.axes1)
% 圆柱体体参数
% R1=10; % 圆柱体半径 m
D1=30; % 圆柱体埋深 m
for R1=1:30
    if start(1)
           % 圆柱体 理论磁异常
            v1=4*pi*R1^3;
            k=0.2; %磁化率
            M1=k*T/u;  %磁化强度 A/m
            m1=M1*v1;   %磁矩
        is=acot(tan(i)*sec(A));
        ms=m1*(cos(i)^2*cos(A)^2+sin(i)^2);
        Za=(u*ms*((D1.^2-(X-50).^2)*sin(is)-2*D1*(X-50).*cos(is)))./(2*pi*((X-50).^2+D1.^2).^2);
        switch sel
                case 1
                    
                case 2
                    surfc(X,Y,Za);
                    zlabel('\bfZa/nT');
                    xlabel('\bfW<->E','rotation',30);%旋转x轴标签
                    ylabel('\bfN<->S','rotation',-45);
                    xlabel('\bfW<->E','rotation',30);%旋转x轴标签
                    ylabel('\bfN<->S','rotation',-45);
                    title('\bf圆柱体Za异常随半径变化曲面图\fontname{宋体}');
                    set(gca,'FontSize',10,'Fontname', '宋体');  
                    set(handles.R,'string',num2str(R1));
                    shading flat
                    colorbar
                case 3
                    contourf(X,Y,Za);
                    ylabel('\bfS<->N');
                    xlabel('\bfW<->E');
                    title('\bf圆柱体Za异常随半径变化等值线图/nT\fontname{宋体}');
                    set(gca,'FontSize',10,'Fontname', '宋体');  
                    set(handles.R,'string',num2str(R1));
                    shading flat
                    colorbar
         end
            pause(time);
     end
end


function D_Callback(hObject, eventdata, handles)


function D_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function pushbutton5_Callback(hObject, eventdata, handles)
%% 埋深
global time;
global start;
start=[0 1 0 0 0 0];

set(handles.R,'string','10');
set(handles.k,'string','0.2');
set(handles.i,'string','0');
set(handles.a,'string','0');
set(handles.T,'string','50000');
sel=handles.sel;
dx=2; % X方向测点间距
dy=2; % Y方向测点间距
nx=201; % X方向测点数
ny=201; % Y方向测点数
xmin=-200; % X方向起点
ymin=-200; % Y方向起点
x=xmin:dx:(xmin+(nx-1)*dx); % X方向范围
y=ymin:dy:(ymin+(ny-1)*dy); % Y方向范围
[X,Y]=meshgrid(x,y); % 转化为排列
u=4*pi*10^(-7);  %磁导率
i=0;  %有效磁化倾角is
A=0; %剖面磁方位角
T=50000;%地磁场T=50000nT
% 球体参数
R1=10;
% D1=10:5:50; % 球体埋深 m
v1=4*pi*R1^3;
k=0.2; %磁化率
M1=k*T/u;  %磁化强度 A/m
m1=M1*v1;   %磁矩
% 球体 理论磁异常
axes(handles.axes1)
for D1=10:1:50
        if start(2)
            is=acot(tan(i)*sec(A));
            ms=m1*(cos(i)^2*cos(A)^2+sin(i)^2);
            Za=(u*ms*((D1.^2-(X-50).^2)*sin(is)-2*D1*(X-50).*cos(is)))./(2*pi*((X-50).^2+D1.^2).^2);
            switch sel
                case 1
                    
                case 2
                    surfc(X,Y,Za);
                    zlabel('\bfZa/nT');
                    xlabel('\bfW<->E','rotation',30);%旋转x轴标签
                    ylabel('\bfN<->S','rotation',-45);
                    title('\bf圆柱体Za异常随埋深变化曲面图\fontname{宋体}');
                    set(gca,'FontSize',10,'Fontname', '宋体');  
                    set(handles.D,'string',num2str(D1));
                    shading flat
                    colorbar
                case 3
                    contourf(X,Y,Za);
                    ylabel('\bfS<->N');
                    xlabel('\bfW<->E');
                    title('\bf圆柱体Za异常随埋深变化等值线图/nT\fontname{宋体}');
                    set(gca,'FontSize',10,'Fontname', '宋体');  
                    set(handles.D,'string',num2str(D1));
                    shading flat
                    colorbar
            end
            pause(time);
        end
end

function k_Callback(hObject, eventdata, handles)


function k_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function pushbutton6_Callback(hObject, eventdata, handles)
%% 磁化率

global time;
global start;
start=[0 0 1 0 0 0];

set(handles.R,'string','10');
set(handles.D,'string','30');
set(handles.i,'string','0');
set(handles.a,'string','0');
set(handles.T,'string','50000');
sel=handles.sel;
dx=2; % X方向测点间距
dy=2; % Y方向测点间距
nx=201; % X方向测点数
ny=201; % Y方向测点数
xmin=-200; % X方向起点
ymin=-200; % Y方向起点
x=xmin:dx:(xmin+(nx-1)*dx); % X方向范围
y=ymin:dy:(ymin+(ny-1)*dy); % Y方向范围
[X,Y]=meshgrid(x,y); % 转化为排列
u=4*pi*10^(-7);  %磁导率
i=0;  %有效磁化倾角is
A=0; %剖面磁方位角
T=50000;%地磁场T=50000nT
% 球体参数
R1=10;
D1=10; % 球体埋深 m
v1=4*pi*R1^3;
% k=0.2; %磁化率

% 球体 理论磁异常
axes(handles.axes1)
for k=0.2:0.1:2
        if start(3)
            M1=k*T/u;  %磁化强度 A/m
            m1=M1*v1;   %磁矩
            is=acot(tan(i)*sec(A));
            ms=m1*(cos(i)^2*cos(A)^2+sin(i)^2);
            Za=(u*ms*((D1.^2-(X-50).^2)*sin(is)-2*D1*(X-50).*cos(is)))./(2*pi*((X-50).^2+D1.^2).^2);
            switch sel
                case 1
                    
                case 2
                    surfc(X,Y,Za);
                    zlabel('\bfZa/nT');
                    xlabel('\bfW<->E','rotation',30);%旋转x轴标签
                    ylabel('\bfN<->S','rotation',-45);
                    title('\bf圆柱体Za异常随磁化率变化曲面图\fontname{宋体}');
                    set(gca,'FontSize',10,'Fontname', '宋体');  
                    set(handles.k,'string',num2str(k));
                    shading flat
                    colorbar
                case 3
                    contourf(X,Y,Za);
                    ylabel('\bfS<->N');
                    xlabel('\bfW<->E');
                    title('\bf圆柱体Za异常随磁化率变化等值线图/nT\fontname{宋体}');
                    set(gca,'FontSize',10,'Fontname', '宋体');  
                    set(handles.k,'string',num2str(k));
                    shading flat
                    colorbar
            end
            pause(time);
        end
end

function i_Callback(hObject, eventdata, handles)


function i_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function pushbutton7_Callback(hObject, eventdata, handles)
%% 磁化强度倾角
global time;
global start;
start=[0 0 0 1 0 0];

set(handles.R,'string','10');
set(handles.D,'string','30');
set(handles.k,'string','0.2');
set(handles.a,'string','0');
set(handles.T,'string','50000');
sel=handles.sel;
dx=2; % X方向测点间距
dy=2; % Y方向测点间距
nx=201; % X方向测点数
ny=201; % Y方向测点数
xmin=-200; % X方向起点
ymin=-200; % Y方向起点
x=xmin:dx:(xmin+(nx-1)*dx); % X方向范围
y=ymin:dy:(ymin+(ny-1)*dy); % Y方向范围
[X,Y]=meshgrid(x,y); % 转化为排列
u=4*pi*10^(-7);  %磁导率
% i=0;  %有效磁化倾角is
A=0; %剖面磁方位角
T=50000;%地磁场T=50000nT
% 球体参数
R1=10;
D1=10; % 球体埋深 m
v1=4*pi*R1^3;
k=0.2; %磁化率

% 球体 理论磁异常
axes(handles.axes1)
for i=0:15:360
        if start(4)
            
                M1=k*T/u;  %磁化强度 A/m
                m1=M1*v1;   %磁矩
                is=acot(tan(i)*sec(A));
                ms=m1*(cos(i)^2*cos(A)^2+sin(i)^2);
                Za=(u*ms*((D1.^2-(X-50).^2)*sin(is)-2*D1*(X-50).*cos(is)))./(2*pi*((X-50).^2+D1.^2).^2);
                switch sel
                    case 1
                    
                    case 2
                        surfc(X,Y,Za);
                        zlabel('\bfZa/nT');
                        xlabel('\bfW<->E','rotation',30);%旋转x轴标签
                        ylabel('\bfN<->S','rotation',-45);
                        title('\bf圆柱体Za异常随磁化强度倾角变化曲面图\fontname{宋体}');
                        set(gca,'FontSize',10,'Fontname', '宋体');  
                        set(handles.i,'string',num2str(i));
                        shading flat
                        colorbar
                    case 3
                        contourf(X,Y,Za);
                        ylabel('\bfS<->N');
                        xlabel('\bfW<->E');
                        title('\bf圆柱体Za异常随磁化强度倾角变化等值线图/nT\fontname{宋体}');
                        set(gca,'FontSize',10,'Fontname', '宋体');  
                        set(handles.i,'string',num2str(i));
                        shading flat
                        colorbar
                 end
            pause(time);
           
        end
end


function a_Callback(hObject, eventdata, handles)


function a_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function pushbutton8_Callback(hObject, eventdata, handles)
%% 剖面磁方位角
global time;
global start;
start=[0 0 0 0 1 0];

set(handles.R,'string','10');
set(handles.D,'string','30');
set(handles.k,'string','0.2');
set(handles.i,'string','0');
set(handles.T,'string','50000');
sel=handles.sel;
dx=2; % X方向测点间距
dy=2; % Y方向测点间距
nx=201; % X方向测点数
ny=201; % Y方向测点数
xmin=-200; % X方向起点
ymin=-200; % Y方向起点
x=xmin:dx:(xmin+(nx-1)*dx); % X方向范围
y=ymin:dy:(ymin+(ny-1)*dy); % Y方向范围
[X,Y]=meshgrid(x,y); % 转化为排列
u=4*pi*10^(-7);  %磁导率
i=0;  %有效磁化倾角is
% A=0; %剖面磁方位角
T=50000;%地磁场T=50000nT
% 球体参数
R1=10;
D1=10; % 球体埋深 m
v1=4*pi*R1^3;
k=0.2; %磁化率

% 球体 理论磁异常
axes(handles.axes1)
for A=0:15:360
        if start(5)
            
                M1=k*T/u;  %磁化强度 A/m
                m1=M1*v1;   %磁矩
                is=acot(tan(i)*sec(A));
                ms=m1*(cos(i)^2*cos(A)^2+sin(i)^2);
                Za=(u*ms*((D1.^2-(X-50).^2)*sin(is)-2*D1*(X-50).*cos(is)))./(2*pi*((X-50).^2+D1.^2).^2);
                switch sel
                    case 1
                    
                    case 2
                        surfc(X,Y,Za);
                        zlabel('\bfZa/nT');
                        xlabel('\bfW<->E','rotation',30);%旋转x轴标签
                        ylabel('\bfN<->S','rotation',-45);
                        title('\bf圆柱体Za异常随剖面磁化倾角变化曲面图\fontname{宋体}');
                        set(gca,'FontSize',10,'Fontname', '宋体');  
                        set(handles.a,'string',num2str(A));
                        shading flat
                        colorbar
                    case 3
                        contourf(X,Y,Za);
                        ylabel('\bfS<->N');
                        xlabel('\bfW<->E');
                        title('\bf圆柱体Za异常随剖面磁化倾角变化等值线图/nT\fontname{宋体}');
                        set(gca,'FontSize',10,'Fontname', '宋体');  
                        set(handles.a,'string',num2str(A));
                        shading flat
                        colorbar
                 end
            pause(time);
           
        end
end


function T_Callback(hObject, eventdata, handles)


function T_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function pushbutton9_Callback(hObject, eventdata, handles)
%% 地磁场强度
global time;
global start;
start=[0 0 0 0 0 1];

set(handles.R,'string','10');
set(handles.D,'string','30');
set(handles.k,'string','0.2');
set(handles.i,'string','0');
set(handles.a,'string','0');
sel=handles.sel;
dx=2; % X方向测点间距
dy=2; % Y方向测点间距
nx=201; % X方向测点数
ny=201; % Y方向测点数
xmin=-200; % X方向起点
ymin=-200; % Y方向起点
x=xmin:dx:(xmin+(nx-1)*dx); % X方向范围
y=ymin:dy:(ymin+(ny-1)*dy); % Y方向范围
[X,Y]=meshgrid(x,y); % 转化为排列
u=4*pi*10^(-7);  %磁导率
i=0;  %有效磁化倾角is
A=0; %剖面磁方位角
T=50000;%地磁场T=50000nT
% 球体参数
R1=10;
D1=10; % 球体埋深 m
v1=4*pi*R1^3;
k=0.2; %磁化率

% 球体 理论磁异常
axes(handles.axes1)
for T=45000:200:55000
        if start(6)
            
                M1=k*T/u;  %磁化强度 A/m
                m1=M1*v1;   %磁矩
                is=acot(tan(i)*sec(A));
                ms=m1*(cos(i)^2*cos(A)^2+sin(i)^2);
                Za=(u*ms*((D1.^2-(X-50).^2)*sin(is)-2*D1*(X-50).*cos(is)))./(2*pi*((X-50).^2+D1.^2).^2);
                switch sel
                    case 1
                    
                    case 2
                        surfc(X,Y,Za);
                        zlabel('\bfZa/nT');
                        xlabel('\bfW<->E','rotation',30);%旋转x轴标签
                        ylabel('\bfN<->S','rotation',-45);
                        title('\bf圆柱体Za异常随地磁场强度变化曲面图\fontname{宋体}');
                        set(gca,'FontSize',10,'Fontname', '宋体');  
                        set(handles.T,'string',num2str(T));
                        shading flat
                        colorbar
                    case 3
                        contourf(X,Y,Za);
                        ylabel('\bfS<->N');
                        xlabel('\bfW<->E');
                        title('\bf圆柱体Za异常随地磁场强度变化等值线图/nT\fontname{宋体}');
                        set(gca,'FontSize',10,'Fontname', '宋体');  
                        set(handles.T,'string',num2str(T));
                        shading flat
                        colorbar
                 end
            pause(time);
           
        end
end
function time_Callback(hObject, eventdata, handles)


function time_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function pushbutton10_Callback(hObject, eventdata, handles)
global time
time=str2double(get(handles.time,'string'));


function pushbutton12_Callback(hObject, eventdata, handles)
%% 保存图片
    new_f_handle=figure('visible','off'); %新建一个不可见的figure
    new_axes=copyobj(handles.axes1,new_f_handle); %axes1是GUI界面内要保存图线的Tag，将其copy到不可见的figure中
    set(new_axes,'Units','normalized','Position',[0.1 0.1 0.8 0.8]);%将图线缩放，units是计量单位，position =  [x y width height]
    [filename, pathname ,fileindex]=uiputfile({'*.png';'*.bmp';'*.jpg';'*.eps';},'图片保存为');
    if  filename~=0%未点“取消”按钮或未关闭
        file=strcat(pathname,filename);
        switch fileindex %根据不同的选择保存为不同的类型        
            case 1
                print(new_f_handle,'-dpng',file);% print(new_f_handle,'-dpng',filename);效果一样，将图像打印到指定文件中
                msgbox({'图片保存成功：',pathname,filename},'提示')
%                 fprintf('>>已保存到：%s\n',file);
            case 2
                print(new_f_handle,'-dbmp',file);
                msgbox({'图片保存成功：',pathname,filename},'提示')
%                 fprintf('>>已保存到：%s\n',file);
            case 3
                print(new_f_handle,'-djpeg',file);
                msgbox({'图片保存成功：',pathname,filename},'提示')
%                 fprintf('>>已保存到：%s\n',file);
            case 4
                print(new_f_handle,'-depsc',file);
                msgbox({'图片保存成功：',pathname,filename},'提示')
%                 fprintf('>>已保存到：%s\n',file);
        end 
    else 
        msgbox('保存失败！','massage');    
    end

% [filename,pathname]=uiputfile({'*.bmp';'*.png';'*.jpg'},'保存图片');
% if ~isequal(filename,0)
%     
%     str=[pathname filename];
%     px=getframe(handles.axes1);
%     imwrite(px.cdata,str,'bmp');
%     msgbox({'图片保存成功',pathname,filename},'提示')
% else 
%     msgbox('图片保存失败！');
% end


function pushbutton13_Callback(hObject, eventdata, handles)
global start
start=[0 0 0 0 0 0];
