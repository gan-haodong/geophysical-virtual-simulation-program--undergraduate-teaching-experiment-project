clear ;clc
%% 球体模型正演
% 参数设置
G = 6.672*10^(-11);%万有引力常数
R = 50;%球体半径 
H = 100;%球体圆心的深度（>半径）
rho = 10;%球体剩余密度=周围介质密度-球体密度
M = rho*3/4*pi*R^3;%球体剩余密度=剩余质量*球体体积
x = linspace(-200,200,401);%x方向采样点坐标
y = linspace(-200,200,401);%y方向采样点坐标
delta_g = zeros(401,401);%初始化重力异常值
for i=1:401
    for j=1:401
        delta_g(i,j) = G*M*(H./(x(i)^2+y(j)^2+H^2)^1.5);
    end
end
%% 绘制主剖面图和平面等值线图
figure(1)
contour(x,y,delta_g,'showText','on')
% colormap('gray')
xlabel('X/m')
ylabel('Y/m')
str = sprintf('球体重力异常等值线图\nR=%.1f rho=%.1f H=%.1f',R,rho,H);
title(str)
figure(2)
plot(x,delta_g(:,200),'-b')