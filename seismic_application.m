function varargout = seismic_application(varargin)
% SEISMIC_APPLICATION MATLAB code for seismic_application.fig
%      SEISMIC_APPLICATION, by itself, creates a new SEISMIC_APPLICATION or raises the existing
%      singleton*.
%
%      H = SEISMIC_APPLICATION returns the handle to a new SEISMIC_APPLICATION or the handle to
%      the existing singleton*.
%
%      SEISMIC_APPLICATION('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in SEISMIC_APPLICATION.M with the given input arguments.
%
%      SEISMIC_APPLICATION('Property','Value',...) creates a new SEISMIC_APPLICATION or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before seismic_application_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to seismic_application_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help seismic_application

% Last Modified by GUIDE v2.5 11-Mar-2020 17:09:01

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @seismic_application_OpeningFcn, ...
                   'gui_OutputFcn',  @seismic_application_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before seismic_application is made visible.
function seismic_application_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to seismic_application (see VARARGIN)

% Choose default command line output for seismic_application
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes seismic_application wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = seismic_application_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


function pushbutton1_Callback(hObject, eventdata, handles)
set(gcf,'visible','off')
set(seismic,'visible','on')


function edit1_Callback(hObject, eventdata, handles)


function edit1_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function axes1_CreateFcn(hObject, eventdata, handles)
%% 去除坐标轴

set(gca,'XColor',get(gca,'Color')) ;% 这两行代码功能：将坐标轴和坐标刻度转为白色
set(gca,'YColor',get(gca,'Color'));
set(gca,'XTickLabel',[]); % 这两行代码功能：去除坐标刻度
set(gca,'YTickLabel',[]);


function popupmenu1_Callback(hObject, eventdata, handles)
choose = get(handles.popupmenu1,'value');
axes(handles.axes1)
switch choose
    case 1
    case 2
        img = imread('seismic_figure\seismicApplication1.jpg');
        imshow(img); 
    case 3
        img = imread('seismic_figure\seismicApplication2.jpg');
        imshow(img);
    case 4
        img = imread('seismic_figure\seismicApplication3.jpg');
        imshow(img);
    case 5
        img = imread('seismic_figure\seismicApplication4.jpg');
        imshow(img);
end

function popupmenu1_CreateFcn(hObject, eventdata, handles)

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
