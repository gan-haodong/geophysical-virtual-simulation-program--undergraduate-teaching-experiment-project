function varargout = seismic_forward(varargin)
% SEISMIC_FORWARD MATLAB code for seismic_forward.fig
%      SEISMIC_FORWARD, by itself, creates a new SEISMIC_FORWARD or raises the existing
%      singleton*.
%
%      H = SEISMIC_FORWARD returns the handle to a new SEISMIC_FORWARD or the handle to
%      the existing singleton*.
%
%      SEISMIC_FORWARD('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in SEISMIC_FORWARD.M with the given input arguments.
%
%      SEISMIC_FORWARD('Property','Value',...) creates a new SEISMIC_FORWARD or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before seismic_forward_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to seismic_forward_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help seismic_forward

% Last Modified by GUIDE v2.5 11-Mar-2020 12:58:23

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @seismic_forward_OpeningFcn, ...
                   'gui_OutputFcn',  @seismic_forward_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before seismic_forward is made visible.
function seismic_forward_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to seismic_forward (see VARARGIN)

% Choose default command line output for seismic_forward
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes seismic_forward wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = seismic_forward_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


function V_Callback(hObject, eventdata, handles)


function V_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function H_Callback(hObject, eventdata, handles)


function H_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function F_Callback(hObject, eventdata, handles)


function F_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function dt_Callback(hObject, eventdata, handles)


function dt_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function N_Callback(hObject, eventdata, handles)


function N_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function pushbutton1_Callback(hObject, eventdata, handles)
V = str2num(get(handles.V,'string'));%地层速度
H = str2num(get(handles.H,'string'));%地层厚度
F = str2num(get(handles.F,'string'));%子波频率
dt = str2num(get(handles.dt,'string'));%采样间隔
N = str2num(get(handles.N,'string'));%采样点数
LAYER_REF = zeros(3,1);%反射系数序列
LAYER_NP = zeros(3,1);

%% 计算反射时间
for i=1:3
    LAYER_REF(i)=(V(i+1)-V(i))/(V(i)+V(i+1));
end
%% 计算地震波双程时间

for i=1:3
    LAYER_NP(i)=2*round(H(i)/V(i)/dt);
end
%% 计算每层的反射时间

INTERFACE_NP=zeros(4,1);
for i=2:4
    INTERFACE_NP(i)=INTERFACE_NP(i)+LAYER_NP(i-1);
end
%% 

NW=INTERFACE_NP(4)+1000; %计算采样点数
NY=2*round(1.0/F/dt);%子波长度
LC=NY+NW-1; %褶积后长度
NDEL=round((NY+1)/2-1);%褶积后截取数据起点
WV = zeros(NW,1);
%% 深时转换后给定界面的信息
for i=2:4
    WV(INTERFACE_NP(i))=LAYER_REF(i-1);
end
%% 
rickper=1./F;
n=2*round(rickper/dt);
for i=1:n
    T=(i*dt-rickper);
    rickth=pi*F*T;
    temp=exp(-rickth*rickth);
    WAVELET(i)=temp*(1-2*rickth*rickth);
end

%% 
LA = NY;
A = WAVELET;
LB = NW;
B = WV;
D=zeros(LB,1);
C=zeros(LA+LB-1,1);
for i=1:LA
    for j=1:LB
        K=i+j-1;
        C(K)=C(K)+A(i)*B(j);
    end
end
% 褶积后数据截取
for  i=1:LB
    D(i)=C(i+NDEL);
end

%% 
MAXD=max(D);%计算信号最大值
% for i=1:LB
%     mt = 1000*dt*(i-1);
% end
nx = 1:LB;
s1 = D;
s2 = D+2*MAXD;
s3 = D+4*MAXD;
s4 = D+6*MAXD;
s5 = D+8*MAXD;
axes(handles.axes1)
plot(nx,s1,nx,s2,nx,s3,nx,s4,nx,s5,'linewidth',1.5);
axis([1,LB,-0.4,2.2])
xlabel('\bf采样点')
ylabel('\bf五道地震波记录')
legend('第一道','第二道','第三道','第四道','第五道')
% plot(s1,nx,s2,nx,s3,nx,s4,nx,s5,nx,'linewidth',1.5);
% axis([-0.4,2,1,LB])
grid on


function pushbutton2_Callback(hObject, eventdata, handles)
%% 保存图片
 new_f_handle=figure('visible','off'); %新建一个不可见的figure
    new_axes=copyobj(handles.axes1,new_f_handle); %axes2是GUI界面内要保存图线的Tag，将其copy到不可见的figure中
    set(new_axes,'Units','normalized','Position',[0.1 0.1 0.8 0.8]);%将图线缩放
    [filename, pathname ,fileindex]=uiputfile({'*.png';'*.bmp';'*.jpg';'*.eps';},'图片保存为');
    if  filename~=0%未点“取消”按钮或未关闭
        file=strcat(pathname,filename);
        switch fileindex %根据不同的选择保存为不同的类型        
            case 1
                print(new_f_handle,'-dpng',file);% print(new_f_handle,'-dpng',filename);效果一样，将图像打印到指定文件中
                msgbox({'图片保存成功：',pathname,filename},'提示')
%                 fprintf('>>已保存到：%s\n',file);
            case 2
                print(new_f_handle,'-dbmp',file);
                msgbox({'图片保存成功：',pathname,filename},'提示')
%                 fprintf('>>已保存到：%s\n',file);
            case 3
                print(new_f_handle,'-djpeg',file);
                msgbox({'图片保存成功：',pathname,filename},'提示')
%                 fprintf('>>已保存到：%s\n',file);
            case 4
                print(new_f_handle,'-depsc',file);
                msgbox({'图片保存成功：',pathname,filename},'提示')
%                 fprintf('>>已保存到：%s\n',file);
        end 
    else 
        msgbox('保存失败！','massage');    
    end

function pushbutton3_Callback(hObject, eventdata, handles)
set(gcf,'visible','off')
set(seismic,'visible','on')
