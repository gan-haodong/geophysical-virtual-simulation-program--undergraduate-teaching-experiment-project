dx=0.2;
rho =[10 100 10 500];
h =[20 2 50];
rm = logspace(0,5,60);
Um =DC1D_Forward(rho,h,rm);
rn = rm+dx;
Un =DC1D_Forward(rho,h,rn);
%2��
kam = 2*3.1415926.*rm;
rho_am  = kam.*Um;
loglog(rm,rho_am,'g+-');hold on;
%3��
kamn =2*3.1415926.*rm.*rn./(rn-rm);
rho_amn = kamn.*(Um-Un);
loglog((rm+rn)/2,rho_amn,'b*-');hold on;
%4��
kamnb =3.1415926.*rm.*rn./(rn-rm);
Umm=Um-Un; Unn=-Um+Un;
rho_amnb = kamnb.*(Umm-Unn);
loglog((rm+rn)/2,rho_amnb,'kd-');hold on;
%ż��
kabmn =2*3.1415926.*rm.*rn.*(rm-dx).*(rn-dx)./(rn-rm)...
    ./(rm.*rn-(rm-dx).*(rn-dx));
rbm =rm-dx;rbn =rm;
Ubm =DC1D_Forward(rho,h,rbm);
Ummm =Um-Ubm; Unnn =Un-Um;
rho_abmn = abs(kabmn.*(Ummm-Unnn));
loglog((rm+rn)/2,rho_abmn,'go-');hold on;
xlabel('x/m');ylabel('rhos');
legend('pole-pole array','pole-dipole array',...
    'symmetrical four-pole array','dipole-dipole array');
