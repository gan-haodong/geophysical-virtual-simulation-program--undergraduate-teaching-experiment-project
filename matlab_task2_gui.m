function varargout = matlab_task2_gui(varargin)
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @matlab_task2_gui_OpeningFcn, ...
                   'gui_OutputFcn',  @matlab_task2_gui_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
function matlab_task2_gui_OpeningFcn(hObject, eventdata, handles, varargin)
handles.output = hObject;
% tu_biao=importdata('G:\我的大学\物探虚拟仿真软件\Material\001.jpg');%将33.jpg改成你要添加的图标的名称
% set(handles.pushbutton1,'CDATA',tu_biao);
guidata(hObject, handles);


function varargout = matlab_task2_gui_OutputFcn(hObject, eventdata, handles) 
varargout{1} = handles.output;

function edit1_Callback(hObject, eventdata, handles)
function edit1_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
global ok
ok=false;
function pushbutton1_Callback(hObject, eventdata, handles)
%% 确定
global ok
ok=true;
dx=str2double(get(handles.edit4,'string')); % X方向测点间距/m
% nx=str2double(get(handles.edit5,'string')); % X方向测点数
xmax=str2double(get(handles.edit3,'string')); % X方向起点/m
xmin=str2double(get(handles.edit11,'string'));
cir=str2double(get(handles.edit12,'string'));
% nx=1+(xmax-xmin)/dx;
nx = size(xmin:dx:xmax,2);
x=linspace(xmin,xmax,nx);
A=0;
I=cir*pi/180;
R1=str2double(get(handles.edit8,'string'));
R2=str2double(get(handles.edit9,'string'));
r1=str2double(get(handles.edit6,'string'));
r2=str2double(get(handles.edit7,'string'));
D=str2double(get(handles.edit10,'string'));
B=50000;
k=0.015;
vol1 = 4*pi*r1*r1*r1/3;
vol2 = 4*pi*r2*r2*r2/3;
mu_0 = 4*pi*1e-7;
M=k*B/mu_0;  
ms1=M*vol1;
ms2=M*vol2;
Za1=(mu_0/(4*pi))*ms1*((2*R1*R1-(x-D).*(x-D))*sin(I) ...
-3*R1*(x-D)*cos(I)*cos(A))./((R1^2+(x-D).^2).^2.5);
Za2=(mu_0/(4*pi))*ms2*((2*R2*R2-(x+D).*(x+D))*sin(I) ...
-3*R2*(x+D)*cos(I)*cos(A))./((R2^2+(x+D).^2).^2.5);
Za=Za1+Za2;
%%%%% Za向上延拓5m
Za_up1=zeros(1,nx);
H=str2double(get(handles.edit1,'string'));
if(mod(H,dx)~=0)
    warndlg('上延高度必须为dx的整数倍！！！','运行出错');
else
    n=10;
    h=H/dx;
for i=(h*n+1):(nx-h*n)
    	tmp=0;
    	for j=(i-h*n):h:(i+h*n)
        	k=(j-i)/h;     
        	tmp=tmp+Za(j)*atan(8/(4*k*k+15))/pi;
    	end
    	Za_up1(i)=tmp; 
end
handles.Za=Za;
handles.Za_up=Za_up1;
handles.X=x;
handles.dx=dx;
handles.H=H;
guidata(hObject,handles);
axes(handles.axes1)
plot(x,Za,'b',x,Za_up1,'r--','linewidth',1.5)
grid on
xlabel('x/m'); ylabel('Z_a/nT');
legend('原始异常',['上延', num2str(H),'米']);
hold on
x1=D;
x2=-D;
y1=-R1;
y2=-R2;
theta=0:2*pi/3600:2*pi;
Circle11=x1+r1*cos(theta);
Circle12=y1+r1*sin(theta);
Circle21=x2+r2*cos(theta);
Circle22=y2+r2*sin(theta);
plot(Circle11,Circle12,'m','Linewidth',1);
patch(Circle11,Circle12,'b','FaceAlpha',0.5)
plot(Circle21,Circle22,'m','Linewidth',1);
patch(Circle21,Circle22,'g','FaceAlpha',0.5)
axis equal
end

function pushbutton3_Callback(hObject, eventdata, handles)
axes(handles.axes1)
cla reset

function edit3_Callback(hObject, eventdata, handles)
function edit3_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit4_Callback(hObject, eventdata, handles)
function edit4_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit5_Callback(hObject, eventdata, handles)
function edit5_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit6_Callback(hObject, eventdata, handles)
function edit6_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit7_Callback(hObject, eventdata, handles)
function edit7_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit8_Callback(hObject, eventdata, handles)
function edit8_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit9_Callback(hObject, eventdata, handles)
function edit9_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit10_Callback(hObject, eventdata, handles)
function edit10_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit11_Callback(hObject, eventdata, handles)
function edit11_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function edit12_Callback(hObject, eventdata, handles)
function edit12_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function pushbutton5_Callback(hObject, eventdata, handles)
set(gcf,'visible','off');
ma_Transformation('visible','on');

function pushbutton4_Callback(hObject, eventdata, handles)
%% 保存图片
    new_f_handle=figure('visible','off'); %新建一个不可见的figure
    new_axes=copyobj(handles.axes1,new_f_handle); %axes1是GUI界面内要保存图线的Tag，将其copy到不可见的figure中
    set(new_axes,'Units','normalized','Position',[0.1 0.1 0.8 0.8]);%将图线缩放，units是计量单位，position =  [x y width height]
    [filename, pathname ,fileindex]=uiputfile({'*.png';'*.bmp';'*.jpg';'*.eps';},'图片保存为');
    if  filename~=0%未点“取消”按钮或未关闭
        file=strcat(pathname,filename);
        switch fileindex %根据不同的选择保存为不同的类型        
            case 1
                print(new_f_handle,'-dpng',file);% print(new_f_handle,'-dpng',filename);效果一样，将图像打印到指定文件中
                msgbox({'图片保存成功：',pathname,filename},'提示')
%                 fprintf('>>已保存到：%s\n',file);
            case 2
                print(new_f_handle,'-dbmp',file);
                msgbox({'图片保存成功：',pathname,filename},'提示')
%                 fprintf('>>已保存到：%s\n',file);
            case 3
                print(new_f_handle,'-djpeg',file);
                msgbox({'图片保存成功：',pathname,filename},'提示')
%                 fprintf('>>已保存到：%s\n',file);
            case 4
                print(new_f_handle,'-depsc',file);
                msgbox({'图片保存成功：',pathname,filename},'提示')
%                 fprintf('>>已保存到：%s\n',file);
        end 
    else 
        msgbox('保存失败！','massage');    
    end


function pushbutton6_Callback(hObject, eventdata, handles)
global ok
if ~ok
    msgbox('请先点击确定','提示')
else
    Za=handles.Za;
    Za_up=handles.Za_up;
    x=handles.X;
    H=handles.H;
    dx=handles.dx;
    n=size(x,2);
    Za_div=zeros(1,n);
    Za_upDiv=zeros(1,n);
    for i=1:n-1
        Za_div(i)=(Za(i+1)-Za(i))/dx;
        Za_upDiv(i)=(Za_up(i+1)-Za_up(i))/dx;
    end
    axes(handles.axes1)
    plot(x,Za_div,'g',x,Za_upDiv,'k--','linewidth',1.5)
    grid on
    xlabel('x/m');
    ylabel('Z_a/nT');
    legend('原始异常一阶导数',['上延', num2str(H),'米一阶导数']);
    hold on
end
