function varargout = ma_forward(varargin)
% MA_FORWARD MATLAB code for ma_forward.fig
%      MA_FORWARD, by itself, creates a new MA_FORWARD or raises the existing
%      singleton*.
%
%      H = MA_FORWARD returns the handle to a new MA_FORWARD or the handle to
%      the existing singleton*.
%
%      MA_FORWARD('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in MA_FORWARD.M with the given input arguments.
%
%      MA_FORWARD('Property','Value',...) creates a new MA_FORWARD or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before ma_forward_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to ma_forward_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help ma_forward

% Last Modified by GUIDE v2.5 11-May-2020 13:55:47

% Begin initialization code - DO NOT EDIT
gui_Singleton = 0;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @ma_forward_OpeningFcn, ...
                   'gui_OutputFcn',  @ma_forward_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before ma_forward is made visible.
function ma_forward_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to ma_forward (see VARARGIN)

% Choose default command line output for ma_forward
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes ma_forward wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = ma_forward_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% handles    structure with handles and user data (see GUIDATA)
set(gcf,'visible','off');
magnetic('visible','on');


% --- Executes on button press in pushbutton2.
function pushbutton2_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%% 球体正演

dx=str2double(get(handles.dxdy,'string'));
dy=dx;% X、Y方向测点间距
xmin=-200;
ymin=xmin;%起始坐标
x=xmin:dx:abs(xmin); % X方向范围
y=ymin:dy:abs(ymin); % Y方向范围
[X,Y]=meshgrid(x,y); % 转化为排列
u=4*pi*10^(-7);  %磁导率
i=pi*str2double(get(handles.i,'string'))/180 ; %有效磁化倾角is
a=pi*str2double(get(handles.a,'string'))/180 ;%剖面磁方位角
T=str2double(get(handles.T,'string'));%地磁场初始为T=50000nT
% 球体参数
R1=str2double(get(handles.R,'string')); % 球体半径 m
D1=str2double(get(handles.D,'string')); % 球体埋深 m
if R1>D1
    msgbox('运行错误，球体半径应不超过中心埋深','提示');
else
    
    v1=4*pi*R1^3/3;
    k=str2double(get(handles.k,'string')); %磁化率
    M1=k*T/u;  %磁化强度 A/m
    m1=M1*v1;   %磁矩
    % 球体 理论磁异常
    Za=(u*m1*((2*D1.^2-X.^2-Y.^2)*sin(i)-3*D1*X.*cos(i)*cos(a)...
    -3*D1*Y.*cos(i)*sin(a)))./(4*pi*(X.^2+Y.^2+D1.^2).^(5/2));
    %主剖面
    axes(handles.axes2)
    switch handles.paintMethod
        case 1
        case 2%曲线图
            cla reset
            ny = floor((size(Za,2)+1)/2);
            size(Za(ny,:))
            plot(x,Za(ny,:),'-r');
            title('\bf理论球体Za磁异常主测线曲线图/nT\fontname{宋体}');
            xyplot;
        case 3%等值线图
            cla reset
            contourf(X,Y,Za);
            xyplot;
            axis equal
            title('\bf理论球体Za磁异常等值线图/nT\fontname{宋体}');
            set(gca,'FontSize',10,'Fontname', '宋体');
            colorbar(handles.axes2)
        case 4%曲面图
            cla reset
            surfc(X,Y,Za)
            xlabel('X/m')
            ylabel('Y/m')
            zlabel('\bfZa磁异常/nT\fontname{宋体}')
            title('\bf理论球体Za磁异常等值线图/nT\fontname{宋体}');
            shading flat
            colorbar(handles.axes2)
    end
    
end

%% 



function R_Callback(hObject, eventdata, handles)
% hObject    handle to R (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of R as text
%        str2double(get(hObject,'String')) returns contents of R as a double


% --- Executes during object creation, after setting all properties.
function R_CreateFcn(hObject, eventdata, handles)
% hObject    handle to R (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function D_Callback(hObject, eventdata, handles)
% hObject    handle to D (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of D as text
%        str2double(get(hObject,'String')) returns contents of D as a double


% --- Executes during object creation, after setting all properties.
function D_CreateFcn(hObject, eventdata, handles)
% hObject    handle to D (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function k_Callback(hObject, eventdata, handles)
% hObject    handle to k (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of k as text
%        str2double(get(hObject,'String')) returns contents of k as a double


% --- Executes during object creation, after setting all properties.
function k_CreateFcn(hObject, eventdata, handles)
% hObject    handle to k (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function i_Callback(hObject, eventdata, handles)
% hObject    handle to i (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of i as text
%        str2double(get(hObject,'String')) returns contents of i as a double


% --- Executes during object creation, after setting all properties.
function i_CreateFcn(hObject, eventdata, handles)
% hObject    handle to i (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function a_Callback(hObject, eventdata, handles)
% hObject    handle to a (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of a as text
%        str2double(get(hObject,'String')) returns contents of a as a double


% --- Executes during object creation, after setting all properties.
function a_CreateFcn(hObject, eventdata, handles)
% hObject    handle to a (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function dxdy_Callback(hObject, eventdata, handles)
% hObject    handle to dxdy (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of dxdy as text
%        str2double(get(hObject,'String')) returns contents of dxdy as a double


% --- Executes during object creation, after setting all properties.
function dxdy_CreateFcn(hObject, eventdata, handles)
% hObject    handle to dxdy (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function min_Callback(hObject, eventdata, handles)
% hObject    handle to min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of min as text
%        str2double(get(hObject,'String')) returns contents of min as a double


% --- Executes during object creation, after setting all properties.
function min_CreateFcn(hObject, eventdata, handles)
% hObject    handle to min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function T_Callback(hObject, eventdata, handles)
% hObject    handle to T (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of T as text
%        str2double(get(hObject,'String')) returns contents of T as a double


% --- Executes during object creation, after setting all properties.
function T_CreateFcn(hObject, eventdata, handles)
% hObject    handle to T (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function save_Callback(hObject, eventdata, handles)
%% 图片保存1

    new_f_handle=figure('visible','off'); %新建一个不可见的figure
    new_axes=copyobj(handles.axes2,new_f_handle); %axes2是GUI界面内要保存图线的Tag，将其copy到不可见的figure中
    set(new_axes,'Units','normalized','Position',[0.1 0.1 0.8 0.8]);%将图线缩放
    [filename, pathname ,fileindex]=uiputfile({'*.png';'*.bmp';'*.jpg';'*.eps';},'图片保存为');
    if  filename~=0%未点“取消”按钮或未关闭
        file=strcat(pathname,filename);
        switch fileindex %根据不同的选择保存为不同的类型        
            case 1
                print(new_f_handle,'-dpng',file);% print(new_f_handle,'-dpng',filename);效果一样，将图像打印到指定文件中
                msgbox({'图片保存成功：',pathname,filename},'提示')
%                 fprintf('>>已保存到：%s\n',file);
            case 2
                print(new_f_handle,'-dbmp',file);
                msgbox({'图片保存成功：',pathname,filename},'提示')
%                 fprintf('>>已保存到：%s\n',file);
            case 3
                print(new_f_handle,'-djpg',file);
                msgbox({'图片保存成功：',pathname,filename},'提示')
%                 fprintf('>>已保存到：%s\n',file);
            case 4
                print(new_f_handle,'-depsc',file);
                msgbox({'图片保存成功：',pathname,filename},'提示')
%                 fprintf('>>已保存到：%s\n',file);
        end 
    else 
        msgbox('保存失败！','massage');
    end

 function pushbutton12_Callback(hObject, eventdata, handles)
     %% 图片保存2

    new_f_handle=figure('visible','off'); %新建一个不可见的figure
    new_axes=copyobj(handles.axes3,new_f_handle); %axes2是GUI界面内要保存图线的Tag，将其copy到不可见的figure中
    set(new_axes,'Units','normalized','Position',[0.1 0.1 0.8 0.8]);%将图线缩放
    [filename, pathname ,fileindex]=uiputfile({'*.png';'*.bmp';'*.jpg';'*.eps';},'图片保存为');
    if  filename~=0%未点“取消”按钮或未关闭
        file=strcat(pathname,filename);
        switch fileindex %根据不同的选择保存为不同的类型        
            case 1
                print(new_f_handle,'-dpng',file);% print(new_f_handle,'-dpng',filename);效果一样，将图像打印到指定文件中
                msgbox({'图片保存成功：',pathname,filename},'提示')
%                 fprintf('>>已保存到：%s\n',file);
            case 2
                print(new_f_handle,'-dbmp',file);
                msgbox({'图片保存成功：',pathname,filename},'提示')
%                 fprintf('>>已保存到：%s\n',file);
            case 3
                print(new_f_handle,'-djpg',file);
                msgbox({'图片保存成功：',pathname,filename},'提示')
%                 fprintf('>>已保存到：%s\n',file);
            case 4
                print(new_f_handle,'-depsc',file);
                msgbox({'图片保存成功：',pathname,filename},'提示')
%                 fprintf('>>已保存到：%s\n',file);
        end 
     else 
        msgbox('保存失败！','massage');   
    end

function pushbutton13_Callback(hObject, eventdata, handles)
%% 圆柱体正演

dx=str2double(get(handles.dxdy2,'string')); % X方向测点间距
dy=dx; % Y方向测点间距
xmin=-200; % X方向起点
ymin=xmin; % Y方向起点
x=xmin:dx:abs(xmin); % X方向范围
y=ymin:dy:abs(ymin); % Y方向范围
[X,Y]=meshgrid(x,y); % 转化为排列
u=4*pi*10^(-7);  %磁导率
i=pi*str2double(get(handles.i2,'string'))/180;  %磁化倾角I
T=str2double(get(handles.T2,'string'));%地磁场T=50000nT
A=str2double(get(handles.A,'string'));
% 圆柱体体参数
R1=str2double(get(handles.R2,'string')); % 圆柱体半径 m
D1=str2double(get(handles.D2,'string')); % 圆柱体埋深 m
if R1>D1
    msgbox('运行错误，圆柱体半径应不超过中心埋深','提示');
else
    v1=4*pi*R1^3;
    k=0.2; %磁化率
    M1=k*T/u;  %磁化强度 A/m
    m1=M1*v1;   %磁矩
    % 圆柱体 理论磁异常
    is=acot(tan(i)*sec(A));
    ms=m1*(cos(i)^2*cos(A)^2+sin(i)^2);
    Za=(u*ms*((D1.^2-(X-50).^2)*sin(is)-2*D1*(X-50).*cos(is)))./(2*pi*((X-50).^2+D1.^2).^2);
    axes(handles.axes3)
    switch handles.paintMethod2
        case 1
        case 2%曲线图
            cla reset
            ny = floor((size(Za,2)+1)/2);
            size(Za(ny,:))
            plot(x,Za(ny,:),'-r');
            title('\bf理论水平圆柱体Za异常等值线图/nT\fontname{宋体}');
            xyplot;
        case 3%等值线图
            cla reset
            contourf(X,Y,Za);
            xyplot;
            axis equal
            title('\bf理论水平圆柱体Za异常等值线图/nT\fontname{宋体}');
            set(gca,'FontSize',10,'Fontname', '宋体');
            colorbar(handles.axes3)
        case 4%曲面图
            cla reset
            surfc(X,Y,Za)
            xlabel('X/m')
            ylabel('Y/m')
            zlabel('\bfZa磁异常/nT\fontname{宋体}')
            title('\bf理论水平圆柱体Za异常等值线图/nT\fontname{宋体}');
            shading flat
            colorbar(handles.axes3)
    end
    
end

function R2_Callback(hObject, eventdata, handles)


function R2_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function D2_Callback(hObject, eventdata, handles)


function D2_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function k2_Callback(hObject, eventdata, handles)


function k2_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function i2_Callback(hObject, eventdata, handles)


function i2_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function A_Callback(hObject, eventdata, handles)


function A_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function dxdy2_Callback(hObject, eventdata, handles)


function dxdy2_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function min2_Callback(hObject, eventdata, handles)


function min2_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function T2_Callback(hObject, eventdata, handles)


function T2_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function Untitled_1_Callback(hObject, eventdata, handles)


function Untitled_2_Callback(hObject, eventdata, handles)
set(gcf,'visible','off')
ma_sphere_forward('visible','on');


function Untitled_3_Callback(hObject, eventdata, handles)
set(gcf,'visible','off')
ma_cylinder_forward('visible','on');


function paintMethod_Callback(hObject, eventdata, handles)
paintMethod = get(hObject,'value');
handles.paintMethod = paintMethod;
guidata(hObject,handles)

function paintMethod_CreateFcn(hObject, eventdata, handles)

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function paintMethod2_Callback(hObject, eventdata, handles)
paintMethod2 = get(hObject,'value');
handles.paintMethod2 = paintMethod2;
guidata(hObject,handles)

function paintMethod2_CreateFcn(hObject, eventdata, handles)

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
