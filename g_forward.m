function varargout = g_forward(varargin)
% G_FORWARD MATLAB code for g_forward.fig
%      G_FORWARD, by itself, creates a new G_FORWARD or raises the existing
%      singleton*.
%
%      H = G_FORWARD returns the handle to a new G_FORWARD or the handle to
%      the existing singleton*.
%
%      G_FORWARD('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in G_FORWARD.M with the given input arguments.
%
%      G_FORWARD('Property','Value',...) creates a new G_FORWARD or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before g_forward_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to g_forward_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help g_forward

% Last Modified by GUIDE v2.5 07-May-2020 11:12:43

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @g_forward_OpeningFcn, ...
                   'gui_OutputFcn',  @g_forward_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before g_forward is made visible.
function g_forward_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to g_forward (see VARARGIN)

% Choose default command line output for g_forward
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes g_forward wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = g_forward_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(gcf,'visible','off');
gravity('visible','on');


function pushbutton4_Callback(hObject, eventdata, handles)
%% 保存图片
 new_f_handle=figure('visible','off'); %新建一个不可见的figure
    new_axes=copyobj(handles.axes2,new_f_handle); %axes2是GUI界面内要保存图线的Tag，将其copy到不可见的figure中
    set(new_axes,'Units','normalized','Position',[0.1 0.1 0.8 0.8]);%将图线缩放
    [filename, pathname ,fileindex]=uiputfile({'*.png';'*.bmp';'*.jpg';'*.eps';},'图片保存为');
    if  filename~=0%未点“取消”按钮或未关闭
        file=strcat(pathname,filename);
        switch fileindex %根据不同的选择保存为不同的类型        
            case 1
                print(new_f_handle,'-dpng',file);% print(new_f_handle,'-dpng',filename);效果一样，将图像打印到指定文件中
                msgbox({'图片保存成功：',pathname,filename},'提示')
%                 fprintf('>>已保存到：%s\n',file);
            case 2
                print(new_f_handle,'-dbmp',file);
                msgbox({'图片保存成功：',pathname,filename},'提示')
%                 fprintf('>>已保存到：%s\n',file);
            case 3
                print(new_f_handle,'-djpeg',file);
                msgbox({'图片保存成功：',pathname,filename},'提示')
%                 fprintf('>>已保存到：%s\n',file);
            case 4
                print(new_f_handle,'-depsc',file);
                msgbox({'图片保存成功：',pathname,filename},'提示')
%                 fprintf('>>已保存到：%s\n',file);
        end 
    else 
        msgbox('保存失败！','massage');    
    end

function pushbutton2_Callback(hObject, eventdata, handles)
%% 保存图片
 new_f_handle=figure('visible','off'); %新建一个不可见的figure
    new_axes=copyobj(handles.axes1,new_f_handle); %axes2是GUI界面内要保存图线的Tag，将其copy到不可见的figure中
    set(new_axes,'Units','normalized','Position',[0.1 0.1 0.8 0.8]);%将图线缩放
    [filename, pathname ,fileindex]=uiputfile({'*.png';'*.bmp';'*.jpg';'*.eps';},'图片保存为');
    if  filename~=0%未点“取消”按钮或未关闭
        file=strcat(pathname,filename);
        switch fileindex %根据不同的选择保存为不同的类型        
            case 1
                print(new_f_handle,'-dpng',file);% print(new_f_handle,'-dpng',filename);效果一样，将图像打印到指定文件中
                msgbox({'图片保存成功：',pathname,filename},'提示')
%                 fprintf('>>已保存到：%s\n',file);
            case 2
                print(new_f_handle,'-dbmp',file);
                msgbox({'图片保存成功：',pathname,filename},'提示')
%                 fprintf('>>已保存到：%s\n',file);
            case 3
                print(new_f_handle,'-djpeg',file);
                msgbox({'图片保存成功：',pathname,filename},'提示')
%                 fprintf('>>已保存到：%s\n',file);
            case 4
                print(new_f_handle,'-depsc',file);
                msgbox({'图片保存成功：',pathname,filename},'提示')
%                 fprintf('>>已保存到：%s\n',file);
        end 
    else 
        msgbox('保存失败！','massage');    
    end


function pushbutton3_Callback(hObject, eventdata, handles)
%% 球体重力

R1=str2double(get(handles.R1,'string'));
H1=str2double(get(handles.H1,'string'));
ro1=str2double(get(handles.ro1,'string'));
dxdy1=str2double(get(handles.dxdy1,'string'));
min1=str2double(get(handles.min1,'string'));

%% 计算
G=6.672*10^(-11);
x=[min1,abs(min1)];
y=x;
delta_p=ro1;
H=H1;
R=R1;
d=dxdy1;
N=(x(2)-x(1))/d+1;
M=(y(2)-y(1))/d+1;
delta_m=4/3*pi*delta_p*R^3;
if R<=H
 for i=1:N
    X=x(1)+d*(i-1);
    for j=1:M
        Y=y(1)+d*(j-1);
        r=sqrt(X^2+Y^2+H^2);
        v_xx(i,j)=(2*X^2-Y^2-H^2)/r^5;
        v_yy(i,j)=(2*Y^2-X^2-H^2)/r^5;
        v_zz(i,j)=(2*H^2-X^2-Y^2)/r^5;
        v_xy(i,j)=3*Y*X/r^5;
        v_xz(i,j)=-3*H*X/r^5;
        v_yz(i,j)=-3*H*Y/r^5;  
        v_zzz(i,j)=3*(2*H^2-3*X^2-Y^2)/r^7;
    end
 end
 V_xz=v_xz*G*delta_m;
 V_xy=v_xy*G*delta_m;
 V_zz=v_zz*G*delta_m;
 V_yy=v_yy*G*delta_m;
 V_yz=v_yz*G*delta_m;
 V_xx=v_xx*G*delta_m;
 V_zzz=v_zzz*G*delta_m;
 x1=linspace(x(1),x(2),N);
 y1=linspace(y(1),y(2),M);
 axes(handles.axes1)
 switch handles.pop1
    case 1
    case 2  
        switch handles.painterMethod
            case 1
            case 2%曲线图
                cla reset
                ny = (size(V_xx,2)+1)/2;
                plot(x1,V_xx(:,ny),'-r')
                xlabel('X/m')
                title('\bf球体重力异常梯度Vxx曲线图/E','fontsize',15)
                xyplot;
            case 3%等值线图
                cla reset
                contourf(x1,y1,V_xx); 
                xlabel('Y/m')
                ylabel('X/m')
                title('\bf球体重力异常梯度Vxx/E等值线图','fontsize',15)
                colorbar(handles.axes1)
                
            case 4%曲面图
                cla reset
                surf(x1,y1,V_xx)
                xlabel('Y/m')
                ylabel('X/m')
                shading flat
                title('\bf球体重力异常梯度Vxx/E曲面图','fontsize',15)
                colorbar(handles.axes1)
        end
        
    case 3
        switch handles.painterMethod
            case 1
            case 2%曲线图
                cla reset
                ny = (size(V_yy,2)+1)/2;
                plot(x1,V_yy(:,ny),'-r')
                xlabel('X/m')
                title('\bf球体重力异常梯度Vyy曲线图/E','fontsize',15)
                xyplot;
            case 3%等值线图
                cla reset
                contourf(x1,y1,V_yy);
                xlabel('Y/m')
                ylabel('X/m')
                title('\bf球体重力异常梯度Vyy/E','fontsize',15)
                colorbar(handles.axes1)
            case 4%曲面图
                cla reset
                surf(x1,y1,V_yy)
                xlabel('Y/m')
                ylabel('X/m')
                shading flat
                title('\bf球体重力异常梯度Vyy/E曲面图','fontsize',15)
                colorbar(handles.axes1)
        end
        
    case 4
        switch handles.painterMethod
            case 1
            case 2%曲线图
                cla reset
                ny = (size(V_zz,2)+1)/2;
                plot(x1,V_zz(:,ny),'-r')
                xlabel('X/m')
                title('\bf球体重力异常梯度Vzz曲线图/E','fontsize',15)
                xyplot;
            case 3%等值线图
                cla reset
                 contourf(x1,y1,V_zz);
                 xlabel('Y/m')
                ylabel('X/m')
                 title('\bf球体重力异常梯度Vzz/E','fontsize',15)
                colorbar(handles.axes1)
            case 4%曲面图
                cla reset
                surf(x1,y1,V_zz)
                xlabel('Y/m')
                ylabel('X/m')
                shading flat
                title('\bf球体重力异常梯度Vzz/E曲面图','fontsize',15)
                colorbar(handles.axes1)
        end
       
    case 5
         switch handles.painterMethod
            case 1
            case 2%曲线图
                cla reset
                ny = (size(V_xy,2)+1)/2;
                plot(x1,V_xy(:,ny),'-r')
                xlabel('X/m')
                title('\bf球体重力异常梯度Vxy曲线图/E','fontsize',15)
                xyplot;
            case 3%等值线图
                cla reset
                 contourf(x1,y1,V_xy);
                 xlabel('Y/m')
                ylabel('X/m')
                 title('\bf球体重力异常梯度Vxy/E','fontsize',15)
                colorbar(handles.axes1)
            case 4%曲面图
                cla reset
                surf(x1,y1,V_xy)
                xlabel('Y/m')
                ylabel('X/m')
                shading flat
                title('\bf球体重力异常梯度Vxy/E曲面图','fontsize',15)
                colorbar(handles.axes1)
        end
        
    case 6
        switch handles.painterMethod
            case 1
            case 2%曲线图
                cla reset
                ny = (size(V_xz,2)+1)/2;
                plot(x1,V_xz(:,ny),'-r')
                xlabel('X/m')
                title('\bf球体重力异常梯度Vxz曲线图/E','fontsize',15)
                xyplot;
            case 3%等值线图
                cla reset
                contourf(x1,y1,V_xz);
                xlabel('Y/m')
                ylabel('X/m')
                title('\bf球体重力异常梯度Vxz/E','fontsize',15)
                colorbar(handles.axes1)
            case 4%曲面图
                cla reset
                surf(x1,y1,V_xz)
                xlabel('Y/m')
                ylabel('X/m')
                shading flat
                title('\bf球体重力异常梯度Vxz/E曲面图','fontsize',15)
                colorbar(handles.axes1)
        end
        
    case 7
        switch handles.painterMethod
            case 1
            case 2%曲线图
                cla reset
                ny = (size(V_yz,2)+1)/2;
                plot(x1,V_yz(:,ny),'-r')
                xlabel('X/m')
                title('\bf球体重力异常梯度Vyz曲线图/E','fontsize',15)
                xyplot;
            case 3%等值线图
                cla reset
                contourf(x1,y1,V_yz);
                xlabel('Y/m')
                ylabel('X/m')
                title('\bf球体重力异常梯度Vyz/E','fontsize',15)
                colorbar(handles.axes1)
            case 4%曲面图
                cla reset
                surf(x1,y1,V_yz)
                xlabel('Y/m')
                ylabel('X/m')
                shading flat
                title('\bf球体重力异常梯度Vyz/E曲面图','fontsize',15)
                colorbar(handles.axes1)
        end
        
    case 8
        switch handles.painterMethod
            case 1
            case 2%曲线图
                cla reset
                ny = (size(V_zzz,2)+1)/2;
                plot(x1,V_zzz(:,ny),'-r')
                xlabel('X/m')
                title('\bf球体重力异常梯度Vzzz曲线图/E','fontsize',15)
                xyplot;
            case 3%等值线图
                cla reset
                contourf(x1,y1,V_zzz);
                xlabel('Y/m')
                ylabel('X/m')
                title('\bf球体重力异常梯度Vzzz/nMKS','fontsize',15)
                colorbar(handles.axes1)
            case 4%曲面图
                cla reset
                surf(x1,y1,V_zzz)
                xlabel('Y/m')
                ylabel('X/m')
                shading flat
                title('\bf球体重力异常梯度Vzzz/E曲面图','fontsize',15)
                colorbar(handles.axes1)
        end
        
 end
else 
    msgbox('球体半径不能大于中心埋深','error');
end


%% 


function R1_Callback(hObject, eventdata, handles)


function R1_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function H1_Callback(hObject, eventdata, handles)


function H1_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function edit3_Callback(hObject, eventdata, handles)


function edit3_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function ro1_Callback(hObject, eventdata, handles)


function ro1_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function edit5_Callback(hObject, eventdata, handles)


function edit5_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function dxdy1_Callback(hObject, eventdata, handles)


function dxdy1_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function min1_Callback(hObject, eventdata, handles)


function min1_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function edit8_Callback(hObject, eventdata, handles)


function edit8_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function pop1_Callback(hObject, eventdata, handles)
pop1=get(hObject,'value');
handles.pop1=pop1;
guidata(hObject,handles);

function pop1_CreateFcn(hObject, eventdata, handles)

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function pushbutton5_Callback(hObject, eventdata, handles)
%% 有限延伸圆柱体
R2=str2double(get(handles.R2,'string'));%圆柱体横截面半径
H2=str2double(get(handles.H2,'string'));%圆柱体埋深
ro2=str2double(get(handles.ro2,'string'));%圆柱体剩余密度
dxdy2=str2double(get(handles.dxdy2,'string'));%测点间距
min2=str2double(get(handles.min2,'string'));%测量起始坐标
L=str2double(get(handles.L,'string'))/2;%圆柱体半延伸长度

%% 计算
G=6.672*10^(-11);
x=[min2,abs(min2)];
y=x;
delta_p=ro2;
H=H2;
R=R2;
D=dxdy2;
N=(x(2)-x(1))/D+1;
M=(y(2)-y(1))/D+1;
lamda=delta_p*pi*R^2;
if R<=H
 for i=1:N
    X=x(1)+D*(i-1);
    for j=1:M
        Y=y(1)+D*(j-1);
        a=(X^2+H^2+(L-Y)^2)^(3/2);
        b=(X^2+H^2+(L+Y)^2)^(3/2);
        c=G*lamda;
        d=(X^2+H^2)^2;
        V_xx(i,j)=c*(L-Y)*((X^2-H^2)*(X^2+(L-Y)^2+H^2)+X^2*(X^2+H^2))/(d*a)+...
            c*(L+Y)*((X^2-H^2)*(X^2+(L+Y)^2+H^2)+X^2*(X^2+H^2))/(d*b);
        V_yy(i,j)=-(L-Y)*c/a-(c*(L+Y)/b);   
        V_zz(i,j)=c*(L-Y)*((H^2-X^2)*(X^2+(L-Y)^2+H^2)+H^2*(X^2+H^2))/(d*a)+...
        c*(L+Y)*((H^2-X^2)*(X^2+(L+Y)^2+H^2)+H^2*(X^2+H^2))/(d*b);
        V_xy(i,j)=X*c/a-X*c/b;
        V_xz(i,j)=c*H*X*(L-Y)*(3*(X^2+H^2)+2*(L-Y)^2)/(a*d)-c*H*X*(L+Y)*(3*(X^2+H^2)+2*(L+Y)^2)/(b*d);
        V_yz(i,j)=c*H*(-1/a+1/b); 
    end
 end
 x1=linspace(x(1),x(2),N);
 y1=linspace(y(1),y(2),M);
 axes(handles.axes2)
 switch handles.pop2
    case 1
    case 2 
        switch handles.painterMethod2
            case 1
            case 2%曲线图
                cla reset
                ny = (size(V_xx,2)+1)/2;
                plot(x1,V_xx(:,ny),'-r')
                xlabel('X/m')
                title('\bf有限延伸水平圆柱体重力异常梯度Vxx/E','fontsize',12)
                xyplot;
            case 3%等值线图
                cla reset
                contourf(y1,x1,V_xx');
                xlabel('X/m')
                ylabel('Y/m')
                title('\bf有限延伸水平圆柱体重力异常梯度Vxx/E','fontsize',12)
                colorbar(handles.axes2)     
            case 4%曲面图
                cla reset
                surf(x1,y1,V_xx)
                xlabel('Y/m')
                ylabel('X/m')
                shading flat
                title('\bf有限延伸水平圆柱体重力异常梯度Vxx/E','fontsize',15)
                colorbar(handles.axes2)
        end  
    case 3
        switch handles.painterMethod2
            case 1
            case 2%曲线图
                cla reset
                ny = (size(V_yy,2)+1)/2;
                plot(x1,V_yy(:,ny),'-r')
                xlabel('X/m')
                title('\bf有限延伸水平圆柱体重力异常梯度Vyy/E','fontsize',12)
                xyplot;
            case 3%等值线图
                cla reset
                contourf(y1,x1,V_yy');
                xlabel('X/m')
                ylabel('Y/m')
                title('\bf有限延伸水平圆柱体重力异常梯度Vyy/E','fontsize',12)
                colorbar(handles.axes2)     
            case 4%曲面图
                cla reset
                surf(x1,y1,V_yy)
                xlabel('Y/m')
                ylabel('X/m')
                shading flat
                title('\bf有限延伸水平圆柱体重力异常梯度Vyy/E','fontsize',15)
                colorbar(handles.axes2)
        end 
    case 4
        switch handles.painterMethod2
            case 1
            case 2%曲线图
                cla reset
                ny = (size(V_zz,2)+1)/2;
                plot(x1,V_zz(:,ny),'-r')
                xlabel('X/m')
                title('\bf有限延伸水平圆柱体重力异常梯度Vzz/E','fontsize',12)
                xyplot;
            case 3%等值线图
                cla reset
                contourf(y1,x1,V_zz');
                xlabel('X/m')
                ylabel('Y/m')
                title('\bf有限延伸水平圆柱体重力异常梯度Vzz/E','fontsize',12)
                colorbar(handles.axes2)     
            case 4%曲面图
                cla reset
                surf(x1,y1,V_zz)
                xlabel('Y/m')
                ylabel('X/m')
                shading flat
                title('\bf有限延伸水平圆柱体重力异常梯度Vzz/E','fontsize',15)
                colorbar(handles.axes2)
        end
    case 5
        switch handles.painterMethod2
            case 1
            case 2%曲线图
                cla reset
                ny = (size(V_xy,2)+1)/2;
                plot(x1,V_xy(:,ny),'-r')
                xlabel('X/m')
                title('\bf有限延伸水平圆柱体重力异常梯度Vxy/E','fontsize',12)
                xyplot;
            case 3%等值线图
                cla reset
                contourf(y1,x1,V_xy');
                xlabel('X/m')
                ylabel('Y/m')
                title('\bf有限延伸水平圆柱体重力异常梯度Vxy/E','fontsize',12)
                colorbar(handles.axes2)     
            case 4%曲面图
                cla reset
                surf(x1,y1,V_xy)
                xlabel('Y/m')
                ylabel('X/m')
                shading flat
                title('\bf有限延伸水平圆柱体重力异常梯度Vxy/E','fontsize',15)
                colorbar(handles.axes2)
        end
    case 6
        switch handles.painterMethod2
            case 1
            case 2%曲线图
                cla reset
                ny = (size(V_yz,2)+1)/2;
                plot(x1,V_yz(:,ny),'-r')
                xlabel('X/m')
                title('\bf有限延伸水平圆柱体重力异常梯度Vyz/E','fontsize',12)
                xyplot;
            case 3%等值线图
                cla reset
                contourf(y1,x1,V_yz');
                xlabel('X/m')
                ylabel('Y/m')
                title('\bf有限延伸水平圆柱体重力异常梯度Vyz/E','fontsize',12)
                colorbar(handles.axes2)     
            case 4%曲面图
                cla reset
                surf(x1,y1,V_yz)
                xlabel('Y/m')
                ylabel('X/m')
                shading flat
                title('\bf有限延伸水平圆柱体重力异常梯度Vyz/E','fontsize',15)
                colorbar(handles.axes2)
        end
    case 7
        switch handles.painterMethod2
            case 1
            case 2%曲线图
                cla reset
                ny = (size(V_xz,2)+1)/2;
                plot(x1,V_xz(:,ny),'-r')
                xlabel('X/m')
                title('\bf有限延伸水平圆柱体重力异常梯度Vxz/E','fontsize',12)
                xyplot;
            case 3%等值线图
                cla reset
                contourf(y1,x1,V_xz');
                xlabel('X/m')
                ylabel('Y/m')
                title('\bf有限延伸水平圆柱体重力异常梯度Vxz/E','fontsize',12)
                colorbar(handles.axes2)     
            case 4%曲面图
                cla reset
                surf(x1,y1,V_xz)
                xlabel('Y/m')
                ylabel('X/m')
                shading flat
                title('\bf有限延伸水平圆柱体重力异常梯度Vxz/E','fontsize',15)
                colorbar(handles.axes2)
        end
 end
else
    msgbox('截面半径不能超过中心埋深','error');
end

%% 


function R2_Callback(hObject, eventdata, handles)


function R2_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function H2_Callback(hObject, eventdata, handles)


function H2_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function ro2_Callback(hObject, eventdata, handles)


function ro2_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function edit12_Callback(hObject, eventdata, handles)


function edit12_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function dxdy2_Callback(hObject, eventdata, handles)


function dxdy2_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function min2_Callback(hObject, eventdata, handles)


function min2_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function edit15_Callback(hObject, eventdata, handles)


function edit15_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function edit16_Callback(hObject, eventdata, handles)


function edit16_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function pop2_Callback(hObject, eventdata, handles)
pop2=get(hObject,'value');
handles.pop2=pop2;
guidata(hObject,handles);

function pop2_CreateFcn(hObject, eventdata, handles)

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function L_Callback(hObject, eventdata, handles)


function L_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function Untitled_1_Callback(hObject, eventdata, handles)


function Untitled_2_Callback(hObject, eventdata, handles)
set(gcf,'visible','off')
g_sphere_forward('visible','on');

function Untitled_3_Callback(hObject, eventdata, handles)
set(gcf,'visible','off')
g_cylinder_forward('visible','on');


function painterMethod_Callback(hObject, eventdata, handles)
painterMethod = get(hObject,'value');
handles.painterMethod = painterMethod;
guidata(hObject,handles);

function painterMethod_CreateFcn(hObject, eventdata, handles)

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function painterMethod_ButtonDownFcn(hObject, eventdata, handles)


function painterMethod2_Callback(hObject, eventdata, handles)
painterMethod2 = get(hObject,'value');
handles.painterMethod2 = painterMethod2;
guidata(hObject,handles)

function painterMethod2_CreateFcn(hObject, eventdata, handles)

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
