function varargout = se_basic_theory(varargin)
% SE_BASIC_THEORY MATLAB code for se_basic_theory.fig
%      SE_BASIC_THEORY, by itself, creates a new SE_BASIC_THEORY or raises the existing
%      singleton*.
%
%      H = SE_BASIC_THEORY returns the handle to a new SE_BASIC_THEORY or the handle to
%      the existing singletonMATLAB*.
%
%      SE_BASIC_THEORY('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in SE_BASIC_THEORY.M with the given input arguments.
%
%      SE_BASIC_THEORY('Property','Value',...) creates a new SE_BASIC_THEORY or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before se_basic_theory_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to se_basic_theory_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help se_basic_theory

% Last Modified by GUIDE v2.5 16-Nov-2019 11:59:22

% Begin initialization code - DO NOT EDIT
gui_Singleton = 0;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @se_basic_theory_OpeningFcn, ...
                   'gui_OutputFcn',  @se_basic_theory_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before se_basic_theory is made visible.
function se_basic_theory_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to se_basic_theory (see VARARGIN)

% Choose default command line output for se_basic_theory
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes se_basic_theory wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = se_basic_theory_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


function figure1_SizeChangedFcn(hObject, eventdata, handles)


function pushbutton1_Callback(hObject, eventdata, handles)
set(gcf,'visible','off')
set(seismic,'visible','on')

function popupmenu3_Callback(hObject, eventdata, handles)
sel=get(hObject,'value');
switch sel
    case 1
    case 2%反射波
        set(gcf,'visible','off')
        set(se_basic_theory_fanshe,'visible','on')
    case 3%折射波
        set(gcf,'visible','off')
        set(se_basic_theory_zheshe,'visible','on')
    case 4%绕射波
        set(gcf,'visible','off')
        set(se_basic_theory_raoshe,'visible','on')
    case 5%面波
        set(gcf,'visible','off')
        set(se_basic_theory_mianbo,'visible','on')
end

function popupmenu3_CreateFcn(hObject, eventdata, handles)

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function popupmenu2_Callback(hObject, eventdata, handles)
sel=get(hObject,'value');
switch sel
    case 1
    case 2%振动图
        set(gcf,'visible','off');
        set(se_basic_theory_zhendong,'visible','on');
    case 3
        set(gcf,'visible','off');
        set(se_basic_theory_bopou,'visible','on');
end

function popupmenu2_CreateFcn(hObject, eventdata, handles)

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function menu1_Callback(hObject, eventdata, handles)
sel_1=get(hObject,'value');
switch sel_1
    case 1
    case 2 %惠更斯-菲涅尔原理
        set(gcf,'visible','off')
        set(se_basic_theory_hui,'visible','on')
    case 3 %斯奈尔定律
        set(gcf,'visible','off')
        set(se_basic_theory_si,'visible','on')
    case 4 %费马原理
        set(gcf,'visible','off')
        set(se_basic_theory_feima,'visible','on')
    case 5 %视速度定理
        set(gcf,'visible','off')
        set(se_basic_theory_sudu,'visible','on')
end
function menu1_CreateFcn(hObject, eventdata, handles)

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function axes1_CreateFcn(hObject, eventdata, handles)
        set(gca,'XColor',get(gca,'Color')) ;% 这两行代码功能：将坐标轴和坐标刻度转为白色
        set(gca,'YColor',get(gca,'Color'));
        set(gca,'XTickLabel',[]); % 这两行代码功能：去除坐标刻度
        set(gca,'YTickLabel',[]);


function axes2_CreateFcn(hObject, eventdata, handles)
        set(gca,'XColor',get(gca,'Color')) ;% 这两行代码功能：将坐标轴和坐标刻度转为白色
        set(gca,'YColor',get(gca,'Color'));
        set(gca,'XTickLabel',[]); % 这两行代码功能：去除坐标刻度
        set(gca,'YTickLabel',[]);


function axes3_CreateFcn(hObject, eventdata, handles)
        set(gca,'XColor',get(gca,'Color')) ;% 这两行代码功能：将坐标轴和坐标刻度转为白色
        set(gca,'YColor',get(gca,'Color'));
        set(gca,'XTickLabel',[]); % 这两行代码功能：去除坐标刻度
        set(gca,'YTickLabel',[]);
