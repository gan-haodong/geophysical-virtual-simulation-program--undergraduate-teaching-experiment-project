function varargout = seismic_Refraction(varargin)
% SEISMIC_REFRACTION MATLAB code for seismic_Refraction.fig
%      SEISMIC_REFRACTION, by itself, creates a new SEISMIC_REFRACTION or raises the existing
%      singleton*.
%
%      H = SEISMIC_REFRACTION returns the handle to a new SEISMIC_REFRACTION or the handle to
%      the existing singleton*.
%
%      SEISMIC_REFRACTION('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in SEISMIC_REFRACTION.M with the given input arguments.
%
%      SEISMIC_REFRACTION('Property','Value',...) creates a new SEISMIC_REFRACTION or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before seismic_Refraction_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to seismic_Refraction_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help seismic_Refraction

% Last Modified by GUIDE v2.5 11-Mar-2020 14:26:42

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @seismic_Refraction_OpeningFcn, ...
                   'gui_OutputFcn',  @seismic_Refraction_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before seismic_Refraction is made visible.
function seismic_Refraction_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to seismic_Refraction (see VARARGIN)

% Choose default command line output for seismic_Refraction
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes seismic_Refraction wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = seismic_Refraction_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


function pushbutton1_Callback(hObject, eventdata, handles)
set(gcf,'visible','off')
set(seismic,'visible','on')
global stop;
stop = true;

function axes1_CreateFcn(hObject, eventdata, handles)
%% 去除坐标轴

set(gca,'XColor',get(gca,'Color')) ;% 这两行代码功能：将坐标轴和坐标刻度转为白色
set(gca,'YColor',get(gca,'Color'));
set(gca,'XTickLabel',[]); % 这两行代码功能：去除坐标刻度
set(gca,'YTickLabel',[]);

function edit1_Callback(hObject, eventdata, handles)


function edit1_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function pushbutton2_Callback(hObject, eventdata, handles)
global stop;
stop = false;
axes(handles.axes1)
set(handles.edit1,'string','在工程地震勘探中，地震折射波法是一种简便经济的勘探方法，在精度要求不高的情况下，它可以为工程地质提供浅层地层起伏变化和速度横向变化资料，以及潜水面的变化资料等，还可以为反射波法地震勘探提供用于静校正的表层速度和低速带起伏变化资料。')
for i=1:3
    if(~stop)
        img = imread(['seismic_figure\seismicRefraction',num2str(i),'.jpg']);
        imshow(img)
        pause(1.5)
    end
end
