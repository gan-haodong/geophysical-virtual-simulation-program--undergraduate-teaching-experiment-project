function varargout = el_show_avi(varargin)
% EL_SHOW_AVI MATLAB code for el_show_avi.fig
%      EL_SHOW_AVI, by itself, creates a new EL_SHOW_AVI or raises the existing
%      singleton*.
%
%      H = EL_SHOW_AVI returns the handle to a new EL_SHOW_AVI or the handle to
%      the existing singleton*.
%
%      EL_SHOW_AVI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in EL_SHOW_AVI.M with the given input arguments.
%
%      EL_SHOW_AVI('Property','Value',...) creates a new EL_SHOW_AVI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before el_show_avi_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to el_show_avi_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help el_show_avi

% Last Modified by GUIDE v2.5 28-Nov-2019 12:55:54

% Begin initialization code - DO NOT EDIT
gui_Singleton = 0;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @el_show_avi_OpeningFcn, ...
                   'gui_OutputFcn',  @el_show_avi_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before el_show_avi is made visible.
function el_show_avi_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to el_show_avi (see VARARGIN)

% Choose default command line output for el_show_avi
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes el_show_avi wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = el_show_avi_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


function figure1_SizeChangedFcn(hObject, eventdata, handles)


function axes1_CreateFcn(hObject, eventdata, handles)
        set(gca,'XColor',get(gca,'Color')) ;% 这两行代码功能：将坐标轴和坐标刻度转为白色
        set(gca,'YColor',get(gca,'Color'));
        set(gca,'XTickLabel',[]); % 这两行代码功能：去除坐标刻度
        set(gca,'YTickLabel',[]);


function pushbutton1_Callback(hObject, eventdata, handles)
set(gcf,'visible','off')
set(electrical,'visible','on')


function pushbutton3_Callback(hObject, eventdata, handles)
show_avi();


function pushbutton2_Callback(hObject, eventdata, handles)
