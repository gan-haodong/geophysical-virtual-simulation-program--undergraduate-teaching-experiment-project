function varargout = ma_display(varargin)
% MA_DISPLAY MATLAB code for ma_display.fig
%      MA_DISPLAY, by itself, creates a new MA_DISPLAY or raises the existing
%      singleton*.
%
%      H = MA_DISPLAY returns the handle to a new MA_DISPLAY or the handle to
%      the existing singleton*.
%
%      MA_DISPLAY('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in MA_DISPLAY.M with the given input arguments.
%
%      MA_DISPLAY('Property','Value',...) creates a new MA_DISPLAY or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before ma_display_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to ma_display_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help ma_display

% Last Modified by GUIDE v2.5 02-Sep-2019 18:48:19

% Begin initialization code - DO NOT EDIT
gui_Singleton = 0;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @ma_display_OpeningFcn, ...
                   'gui_OutputFcn',  @ma_display_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before ma_display is made visible.
function ma_display_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to ma_display (see VARARGIN)

% Choose default command line output for ma_display
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes ma_display wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = ma_display_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%% 

%% 
set(gcf,'visible','off');
magnetic('visible','on');
%% 


% --- Executes on selection change in listbox1.
function listbox1_Callback(hObject, eventdata, handles)
% hObject    handle to listbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns listbox1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from listbox1


% --- Executes during object creation, after setting all properties.
function listbox1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to listbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in popupmenu_display.
function popupmenu_display_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu_display (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu_display contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu_display
%% 演示引导
sel=get(hObject,'value');
switch sel
    case 1
    case 2
        axes(handles.axes1);
        data=imread('地磁要素01.bmp');
        imshow(data);
        msgbox('地磁要素全球分布演示程序打开，请在右侧使用相应功能','massage');
        set(handles.a,'visible','on');
        set(handles.b,'visible','on');
        set(handles.axes2,'visible','on');
        set(handles.explaination,'visible','on');
        set(handles.explaination,'string',...
            '地磁要素(geomagnetic element)表示地球磁场方向和大小的物理量,确定某一点的磁场情况，需要三个要素，常用的是磁倾角、磁偏角和水平分量。');
    case 3
        axes(handles.axes1)
        a=imread('正常场.jpg');
        imshow(a);
        set(handles.a,'visible','off');
        set(handles.b,'visible','off');
        set(handles.axes2,'visible','off');
        set(handles.explaination,'visible','on');
        set(handles.explaination,'string',...
            {'    地磁场可以分为地磁正常场和地磁异常场两个部分，异常场是相对于正常场而言的，所以根据研究对象的不同,正常场的选取也有所不同。例如研究大陆磁异常，则'...
            '将中心偶极子场作为正常地磁场；研究地壳磁场时，以中心偶极子场和大陆磁场之和为正常场。'})
    case 4
        msgbox('请继续选择后面的选项','提示');
        set(handles.a,'visible','off');
        set(handles.b,'visible','off');
        set(handles.axes2,'visible','off');
        set(handles.explaination,'visible','off');
        
    case 5
        set(handles.a,'visible','off');
        set(handles.b,'visible','off');
        set(handles.axes2,'visible','off');
        set(handles.explaination,'visible','on');
        set(handles.explaination,'string',...
            {'    地磁场长期变化总的特征是随时间变化缓慢，周期长。一般变化周期为几年、几十年，有的更长。对地磁场的长期变化，主要是通过世界各地的地磁台长期的、 连'...
            '续的观测数据取其平均来进行研究的。'})
        a=imread('London.jpg');
        imshow(a);
    case 6
        set(handles.a,'visible','off');
        set(handles.b,'visible','off');
        set(handles.axes2,'visible','off');
        set(handles.explaination,'visible','on');
        set(handles.explaination,'string',...
            {'    太阳静日变化是以一个太阳日 24 小时为周期的变化，称为地磁日变。它的变化是依赖于地方太阳时。其基本特点是 :各个地磁要素的周日变化是逐日不停地在进'...
            '行，其中振幅易变、相位几乎不变。白天 (6：00～ 18：00)磁场变化较大，夜间较平静。夏季的变化幅度最大、冬季的幅度最小、春秋季节居中。'})
        a=imread('Z日变.jpg');
        imshow(a);
    case 7
        set(handles.a,'visible','off');
        set(handles.b,'visible','off');
        set(handles.axes2,'visible','off');
        set(handles.explaination,'visible','on');
        set(handles.explaination,'string',...
            {'    磁暴是一种强烈的地磁扰动。 从赤道到极区均可观察到磁暴现象， 而且几乎是全球同时发生。发生时对地磁场水平分量的强度影响特别显著，而对垂直分量影响相'...
            '对小些。因此，通常研究磁暴的形态和特征是通过水平分量强度变化来进行的。'})
        a=imread('磁暴曲线.png');
        imshow(a);
    case 8
        set(handles.a,'visible','off');
        set(handles.b,'visible','off');
        set(handles.axes2,'visible','off');
        set(handles.explaination,'visible','on');
        set(handles.explaination,'string',...
            {'    地磁脉动是一种地磁场的微扰变化， 它具有准周期结构的特点， 一般周期介于 n×10^-1～n×10^2 s,频率范围从毫赫兹到数赫兹，振幅的变化范围为n×10^-1 '...
            '～10^n nT(在强扰动期间也可达数百纳特 )。'})

        a=imread('地磁脉动.png');
        imshow(a);
       
        
%% 
end

% --- Executes during object creation, after setting all properties.
function popupmenu_display_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu_display (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function b_Callback(hObject, eventdata, handles)
%% 地磁要素
%% 调用函数计算
g=[        0       0     0.0     0.0     0.0     0.0     0.0     0.0     0.0     0.0     0.0     0.0     0.0     0.0    
    -29473.7 -1552.5     0.0     0.0     0.0     0.0     0.0     0.0     0.0     0.0     0.0     0.0     0.0     0.0
    -2419.2  3018.2  1674.0     0.0     0.0     0.0     0.0     0.0     0.0     0.0     0.0     0.0     0.0     0.0
     1342.3 -2334.1  2125.9   618.0     0.0     0.0     0.0     0.0     0.0     0.0     0.0     0.0     0.0     0.0
      909.8   813.0   148.8  -348.3    85.1     0.0     0.0     0.0     0.0     0.0     0.0     0.0     0.0     0.0
     -232.1   358.2   197.3  -142.6  -160.5    -4.9     0.0     0.0     0.0     0.0     0.0     0.0     0.0     0.0
       72.2    68.0    75.4  -137.6   -26.1    12.7   -74.3     0.0     0.0     0.0     0.0     0.0     0.0     0.0
       80.8   -75.2    -5.9    48.1    14.6    10.6     0.0     5.7     0.0     0.0     0.0     0.0     0.0     0.0
       24.1     8.4   -15.5    -5.1   -19.9    12.2    11.3   -15.1    -3.3     0.0     0.0     0.0     0.0     0.0
        5.4     9.4     3.4    -5.3     3.1   -12.4    -0.8     8.4    -8.4   -10.1     0.0     0.0     0.0     0.0
       -2.0    -6.3     0.9    -1.1    -0.2     2.5    -0.3     2.2     3.1    -1.0    -2.8     0.0     0.0     0.0
        3.0    -1.5    -2.1     1.6    -0.3     0.5    -0.8     0.4     1.8     0.2     0.8     3.8     0.0     0.0
       -2.1    -0.2     0.3     1.0    -0.7     0.9    -0.1     0.5    -0.4    -0.4     0.2    -0.8     0.0     0.0
       -0.2    -0.9     0.3     0.4    -0.4     1.1    -0.3     0.8    -0.2     0.4     0.0     0.4    -0.3    -0.3];
%此处2012年球谐系数g的值，此处为构造这样的矩阵是方便后续的编程。
h =[0.0     0.0    0.0    0.0    0.0    0.0    0.0    0.0    0.0    0.0    0.0    0.0    0.0    0.0
     0.0 4887.5    0.0    0.0    0.0    0.0    0.0    0.0    0.0    0.0    0.0    0.0    0.0    0.0
    0.0 -2753.9 -601.2    0.0    0.0    0.0    0.0    0.0    0.0    0.0    0.0    0.0    0.0    0.0
     0.0 -143.3  245.9 -541.0    0.0    0.0    0.0    0.0    0.0    0.0    0.0    0.0    0.0    0.0
     0.0  287.2 -208.8  171.6 -310.8    0.0    0.0    0.0    0.0    0.0    0.0    0.0    0.0    0.0
     0.0   45.7  191.9 -116.3    7.5   99.7    0.0    0.0    0.0    0.0    0.0    0.0    0.0    0.0
     0.0  -21.0   40.0   60.5  -67.3    4.7   55.9    0.0    0.0    0.0    0.0    0.0    0.0    0.0
     0.0  -56.6  -20.6    6.2   24.7    5.4  -28.3   -3.0    0.0    0.0    0.0    0.0    0.0    0.0
     0.0   10.9  -19.6   12.9  -16.6   16.9    6.9  -10.0    2.5    0.0    0.0    0.0    0.0    0.0
     0.0  -20.5   11.6   12.8   -7.2    7.4    8.0    2.2   -6.1    7.0    0.0    0.0    0.0    0.0
     0.0    2.8   -0.1    4.7    4.4   -7.2   -1.0   -4.0   -2.0   -2.0   -8.3    0.0    0.0    0.0
     0.0    0.1    1.7   -0.6   -1.8    0.9   -0.4   -2.5   -1.3   -2.1   -1.9   -1.8    0.0    0.0
     0.0   -0.8    0.3    2.2   -2.2    0.5    0.6    0.0    0.1    0.3   -0.9   -0.2    0.8    0.0
     0.0   -0.8    0.3    1.7   -0.6   -1.2   -0.1    0.5    0.1    0.5    0.4   -0.2   -0.5   -0.8];
%此处2012年球谐系数h的值，此处为构造这样的矩阵是方便后续的编程。

X1=0; Y1=0; Z1=0;%此处三个值的赋值是为了让他们的初始迭代值，计算每一个球谐系数下的X，Y，Z分量的磁场的大小
i=0; j=0;  %此处赋值为零是为了控制行数与列数，为了控制X，Y，Z的行，列的值
for theta1=0:0.05:pi   %此处为theta1循环，计算磁场各分量的值
    P(1,1)=1;P(2,1)=cos(theta1);P(2,2)=sin(theta1);  %这是为了计算缔合勒让德函数迭代的初始值
    DP(1,1)=0;DP(2,1)=-sin(theta1);DP(2,2)=cos(theta1);
    for n=2:14  %该循环是为了计算缔合勒让德函数及其一阶导数，其计算方法参看文献[1]柴松均,陈曙东,张爽.国际地磁参考场的计算与软件实现[J]吉林大学学报,2015,33(3),280-284
        for m=1:n+1 %求解缔合勒让德函数
            if m==n+1
                P(m,m)=sqrt((2*(m-1)-1)/(2*(m-1)))*sin(theta1)*P(m-1,m-1);
            else
                P(n+1,m)=((2*n-1)*cos(theta1)*P(n,m)-sqrt((n-1)^2-(m-1)^2)*P(n-1,m))/sqrt((n)^2-(m-1)^2);
            end
        end
        for m=1:n+1 %求解缔合勒让德函数一阶导数
            DP(n+1,m)=((n)*(cos(theta1))*P(n+1,m)-sqrt(n^2-(m-1)^2)*P(n,m))/(sin(theta1));
        end
    end
    i=i+1;    %此处就是在计算第几行，方便控制矩阵X，Y，Z（X，Y，Z是分别是磁场的X，Y，Z分量）的行数
    for phi1=-pi:0.05:pi
        j=j+1;%此处就是在计算第几列，方便控制矩阵X，Y，Z（X，Y，Z是分别是磁场的X，Y，Z分量）的列数
        for n=2:14
            for m=1:n
                X1=X1+(g(n,m)*cos((m-1)*phi1)+h(n,m)*sin((m-1)*phi1))*DP(n,m);%计算X分量的值，
                Y1=Y1+(m-1)*(g(n,m)*sin((m-1)*phi1)-h(n,m)*cos((m-1)*phi1))*P(n,m);      %计算Y分量的值
                Z1=Z1-(n*(g(n,m)*cos((m-1)*phi1)+h(n,m)*sin((m-1)*phi1)))*P(n,m);          %计算Z分量的值
            end
        end
        X(i,j)=X1; Z(i,j)=Z1; Y(i,j)=Y1;%此处为赋值，将每个theta1和phi1下的X1，Y1，Z1对应赋值给X，Y，Z
        X1=0;Y1=0; Z1=0;%求解下一个X1，Y1，Z1的值，将其初始化，使得为零
    end
    j=0;%列循环完了以后，将其初始化，继续下一轮循环
end
a=0.5*pi:-0.05:-0.5*pi;   b=-pi:0.05:pi;   [phi,theta]=meshgrid(b,a); %此处为将phi,theta网格化，为了绘制等值线图用
phi=phi./pi.*180;  
theta=theta./pi.*180;%角度与弧度的转换

D=atan(Y./X)/pi*180;%地磁偏角,并用角度表示
I=atan(Z./sqrt(X.^2+Y.^2))/pi*180; %地磁倾角，并用角度表示
H=sqrt(X.^2+Y.^2); %求磁场的水平分量
F=sqrt(X.^2+Y.^2+Z.^2) ;%求总磁场
%% 绘图


sel=get(hObject,'value');
switch sel
    case 1
    case 2
        axes(handles.axes2)
        cla reset
        m_proj('hammer','clong',0);%选择投影类型
        m_grid('xlabeldir','end','fontsize',10);%绘制网格
        m_coast('patch',[.7 .7 .7],'edgecolor','none');%绘制海岸线并填充成灰色
        hold on;
        [cs,h]=m_contour(phi,theta,F,13,'linewidth',1.5,'ShowText','on');%绘制等值线图
        title('地球磁场强度F等值线图');
        ylabel('\bf磁场强度单位：nT');
        hold off
    case 3
        axes(handles.axes2)
        cla reset
        m_proj('hammer','clong',0);%选择投影类型
        m_grid('xlabeldir','end','fontsize',10);%绘制网格
        m_coast('patch',[.7 .7 .7],'edgecolor','none');%绘制海岸线并填充成灰色
        hold on;
        [cs,h]=m_contour(phi,theta,round(H),13,'linewidth',1.5,'ShowText','on');%绘制等值线图
        title('地球磁场强度水平分量H等值线图');
        ylabel('\bf磁场强度单位：nT')
    case 4
        axes(handles.axes2)
        cla reset
        m_proj('hammer','clong',0);%选择投影类型
        m_grid('xlabeldir','end','fontsize',10);%绘制网格
        m_coast('patch',[.7 .7 .7],'edgecolor','none');%绘制海岸线并填充成灰色
        hold on;
        [cs,h]=m_contour(phi,theta,floor(X),13,'linewidth',1.5,'ShowText','on');%绘制等值线图
        title('地球磁场强度X等值线图');
        ylabel('\bf磁场强度单位：nT');
        axis tight
    case 5
        axes(handles.axes2)
        cla reset
        m_proj('hammer','clong',0);%选择投影类型
        m_grid('xlabeldir','end','fontsize',10);%绘制网格
        m_coast('patch',[.7 .7 .7],'edgecolor','none');%绘制海岸线并填充成灰色
        hold on;
        [cs,h]=m_contour(phi,theta,round(Y),13,'linewidth',1.5,'ShowText','on');%绘制等值线图
        title('地球磁场强度Y等值线图');
        ylabel('\bf磁场强度单位：nT')
    case 6
        axes(handles.axes2)
        cla reset
        m_proj('hammer','clong',0);%选择投影类型
        m_grid('xlabeldir','end','fontsize',10);%绘制网格
        m_coast('patch',[.7 .7 .7],'edgecolor','none');%绘制海岸线并填充成灰色
        hold on;
        [cs,h]=m_contour(phi,theta,round(Z),13,'linewidth',1.5,'ShowText','on');%绘制等值线图
        title('地球磁场强度垂直分量Z等值线图');
        ylabel('\bf磁场强度单位：nT')

    case 7
        axes(handles.axes2)
        cla reset
        m_proj('hammer','clong',0);%选择投影类型
        m_grid('xlabeldir','end','fontsize',10);%绘制网格
        m_coast('patch',[.7 .7 .7],'edgecolor','none');%绘制海岸线并填充成灰色
        hold on;
        [cs,h]=m_contour(phi,theta,round(I),15,'linewidth',1.5,'ShowText','on');%绘制等值线图
        title('地球磁场强度磁倾角I等值线图');
        ylabel('\bf磁倾角单位：度')
    case 8
        axes(handles.axes2)
        cla reset
        m_proj('hammer','clong',0);%选择投影类型
        m_grid('xlabeldir','end','fontsize',10);%绘制网格
        m_coast('patch',[.7 .7 .7],'edgecolor','none');%绘制海岸线并填充成灰色
        hold on;
        [cs,h]=m_contour(phi,theta,round(D),30,'linewidth',1.5,'ShowText','on');%绘制等值线图
        title('地球磁场强度磁偏角D等值线图');
        ylabel('\bf磁偏角单位：度')
end

        

function b_CreateFcn(hObject, eventdata, handles)

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
