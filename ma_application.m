function varargout = ma_application(varargin)
% MA_APPLICATION MATLAB code for ma_application.fig
%      MA_APPLICATION, by itself, creates a new MA_APPLICATION or raises the existing
%      singleton*.
%
%      H = MA_APPLICATION returns the handle to a new MA_APPLICATION or the handle to
%      the existing singleton*.
%
%      MA_APPLICATION('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in MA_APPLICATION.M with the given input arguments.
%
%      MA_APPLICATION('Property','Value',...) creates a new MA_APPLICATION or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before ma_application_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to ma_application_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help ma_application

% Last Modified by GUIDE v2.5 14-Nov-2019 17:57:47

% Begin initialization code - DO NOT EDIT
gui_Singleton = 0;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @ma_application_OpeningFcn, ...
                   'gui_OutputFcn',  @ma_application_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before ma_application is made visible.
function ma_application_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to ma_application (see VARARGIN)

% Choose default command line output for ma_application
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes ma_application wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = ma_application_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% handles    structure with handles and user data (see GUIDATA)
set(gcf,'visible','off');
magnetic('visible','on');


function figure1_SizeChangedFcn(hObject, eventdata, handles)


function popupmenu1_Callback(hObject, eventdata, handles)
sel=get(hObject,'value');
switch sel 
    case 1
    case 2 %配合岩性分区与圈定岩体的视磁化率填图    
        axes(handles.axes1)
        a=imread('ma_application_1.png');
        imshow(a);
    case 3 %根据磁异常推断断裂、破碎带及褶皱
        b=imread('ma_application_2.png');
        imshow(b);
end

function popupmenu1_CreateFcn(hObject, eventdata, handles)

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function axes1_CreateFcn(hObject, eventdata, handles)
        %% 去除坐标轴
        
        set(gca,'XColor',get(gca,'Color')) ;% 这两行代码功能：将坐标轴和坐标刻度转为白色
        set(gca,'YColor',get(gca,'Color'));
        set(gca,'XTickLabel',[]); % 这两行代码功能：去除坐标刻度
        set(gca,'YTickLabel',[]);
        %% 


function popupmenu3_Callback(hObject, eventdata, handles)
sel=get(hObject,'value');
axes(handles.axes3)
switch sel
    case 1
    case 2 %河北某地磁铁矿上的航磁
        a1=imread('ma_application_4.png');
        imshow(a1);
    case 3 %江苏某火山岩铁矿磁异常综合平面图
        a2=imread('ma_application_5.png');
        imshow(a2);
    case 4 %甘肃某镍矿区磁异常图
        a3=imread('ma_application_6.png');
        imshow(a3);
    case 5 %云南某地铬铁矿区磁异常图
        a4=imread('ma_application_7.png');
        imshow(a4);
    case 6 %某地硼矿磁异常图
        a5=imread('ma_application_8.png');
        imshow(a5);
end


function popupmenu3_CreateFcn(hObject, eventdata, handles)

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function popupmenu2_Callback(hObject, eventdata, handles)
sel=get(hObject,'value');
axes(handles.axes2)
switch sel
    case 1
    case 2 %文字介绍①
       set(handles.text4,'visible','on')
       set(handles.axes2,'visible','off')
       set(handles.text4,'string',...
           '1、大多数沉积岩几乎都是无磁性的，而下伏火成岩和基岩通常是有磁性的。2、根据磁测资料确定了基岩的深度，也就确定了沉积物的厚度。3、如果确定了基岩的起伏，因基底面起伏能在上覆沉积岩中产生有利于油气聚集的构造起伏，因而能为油气勘查提供有用资料。')
    case 3 %文字介绍②
        set(handles.text4,'visible','on')
        set(handles.axes2,'visible','off')
        set(handles.text4,'string',...
            '在油气构造普查中的作用，磁测作用：1、能以相对少的投资，在较短时间内提供大面积反映区域与局部构造信息的磁场资料；2、可以比较详细地确定进一步投入比较昂贵的物探方法的工作地段；3、预测油气远景构造')
    case 4 %油气藏上方磁性矿物的磁异常
        set(handles.text4,'visible','off')
        set(handles.axes2,'visible','on')
        a1=imread('ma_application_4_1.png');
        imshow(a1);
    case 5 %油气盆地地质构造勘查
        set(handles.text4,'visible','off')
        set(handles.axes2,'visible','on')
        a2=imread('ma_application_3.png');
        imshow(a2);
end
function popupmenu2_CreateFcn(hObject, eventdata, handles)

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function axes2_CreateFcn(hObject, eventdata, handles)
        %% 去除坐标轴
        
        set(gca,'XColor',get(gca,'Color')) ;% 这两行代码功能：将坐标轴和坐标刻度转为白色
        set(gca,'YColor',get(gca,'Color'));
        set(gca,'XTickLabel',[]); % 这两行代码功能：去除坐标刻度
        set(gca,'YTickLabel',[]);
        %% 
        


function axes3_CreateFcn(hObject, eventdata, handles)
        %% 去除坐标轴
        
        set(gca,'XColor',get(gca,'Color')) ;% 这两行代码功能：将坐标轴和坐标刻度转为白色
        set(gca,'YColor',get(gca,'Color'));
        set(gca,'XTickLabel',[]); % 这两行代码功能：去除坐标刻度
        set(gca,'YTickLabel',[]);
        %% 
        


function popupmenu3_DeleteFcn(hObject, eventdata, handles)
