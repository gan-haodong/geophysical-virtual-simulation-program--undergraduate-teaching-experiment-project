function z1 = hankel_801(fun,B) 
 	 a = -30.0482205425 ;
 	 delta=0.1;
 	 J0=load('J0.dat');
 	 z1 = 0;
 	 for i = 1:size(J0,1)
         m=exp(a+delta*(i-1))/B;
     	 z1 = z1 + feval(fun,m) * J0(i)/m;
 	 end
 	 z1 = z1/B;
