function varargout = magnetic(varargin)
% MAGNETIC MATLAB code for magnetic.fig
%      MAGNETIC, by itself, creates a new MAGNETIC or raises the existing
%      singleton*.
%
%      H = MAGNETIC returns the handle to a new MAGNETIC or the handle to
%      the existing singleton*.
%
%      MAGNETIC('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in MAGNETIC.M with the given input arguments.
%
%      MAGNETIC('Property','Value',...) creates a new MAGNETIC or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before magnetic_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to magnetic_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help magnetic

% Last Modified by GUIDE v2.5 29-Aug-2019 19:38:36

% Begin initialization code - DO NOT EDIT
gui_Singleton = 0;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @magnetic_OpeningFcn, ...
                   'gui_OutputFcn',  @magnetic_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before magnetic is made visible.
function magnetic_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to magnetic (see VARARGIN)

% Choose default command line output for magnetic
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes magnetic wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = magnetic_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;



function edit1_Callback(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit1 as text
%        str2double(get(hObject,'String')) returns contents of edit1 as a double


% --- Executes during object creation, after setting all properties.
function edit1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(gcf,'visible','off');
start_interface('visible','on');


% --- Executes on button press in ma_display.
function ma_display_Callback(hObject, eventdata, handles)
% hObject    handle to ma_display (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(gcf,'visible','off');
ma_display('visible','on');


% --- Executes on button press in ma_method.
function ma_method_Callback(hObject, eventdata, handles)
% hObject    handle to ma_method (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(gcf,'visible','off');
ma_Transformation('visible','on');

% --- Executes on button press in ma_forward.
function ma_forward_Callback(hObject, eventdata, handles)
% hObject    handle to ma_forward (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(gcf,'visible','off');
ma_forward('visible','on');

% --- Executes on button press in ma_inversion.
function ma_inversion_Callback(hObject, eventdata, handles)
% hObject    handle to ma_inversion (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(gcf,'visible','off');
ma_inversion('visible','on');


% --- Executes on button press in ma_application.
function ma_application_Callback(hObject, eventdata, handles)
% hObject    handle to ma_application (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(gcf,'visible','off');
ma_application('visible','on');
